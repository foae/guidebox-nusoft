<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Trait.Helper.php';

/**
 * Class Store
 */
class Store extends CI_Controller
{

    use Helper;

    /**
     * @var string
     */
    public $api_key = 'rK2tDhmJ3GLbbEklrOfw67AXS2xcGdGi'; // Lucian, w, prod

    /**
     * @var string
     */
    public $url = '';

    /**
     * @var string
     */
    public $http_code = '';

    /**
     * @var
     */
    public $last_timestamp;

    /**
     * @var
     */
    public $new_timestamp;

    /**
     * @var string
     */
    public $user_agent;


    /**
     * Store constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Db');

        $this->url = "http://api-public.guidebox.com/v1.43/US/{$this->api_key}/";
        $this->user_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";

        $this->loadOldTimestamp();
        $this->setNewTimestamp();
    }


    /**
     * Main page served on accessing web-root/store
     */
    public function index()
    {
        echo 'Memory: ' . $this->human_filesize(memory_get_usage()) . '. Load: ' . implode(' / ', sys_getloadavg());
    }


    /**
     * Sets the current timestamp to the Guidebox's one
     * @throws Exception
     */
    private function setNewTimestamp()
    {
        $url = $this->url . "updates/get_current_time";
        $this->new_timestamp = $this->_curl($url)['results'];
        file_put_contents('timestamp', $this->new_timestamp);
        usleep(250000);
    }

    /**
     * Load the last timestamp from local storage
     */
    private function loadOldTimestamp()
    {
        if (!file_exists('timestamp')) {
            file_put_contents('timestamp', $this->_curl($this->url . "updates/get_current_time")['results']);
        }

        $this->last_timestamp = file_get_contents('timestamp');
    }
    public function testt(){
      echo "this is a test";
    }
    public function checkShowIntegrity(){
      $shows = $this->db->query("SELECT id FROM shows")->result_array();
      foreach($shows as $show){
        $this->updateShowToEpisodes($show['id']);
      }
    }
	public function syncmovie($mid)
    {

		//$status = $this->deleteMovie($mid);
		//log_message('info', "[MOVIE CHANGED] Movie deleted, movie ID: {$mid} +++");

		//$this->processMovieById($mid);
		//log_message('info', "[MOVIE CHANGED] Movie data updated, all data repulled from Guidebox, movie ID: {$mid} +++");

		$this->processMovieTrailers($mid);
		log_message('info', "[MOVIE CHANGED] Movie trailers updated, all data repulled from Guidebox, movie ID: {$mid} +++");

		$this->processMovieImages($mid);
		log_message('info', "[MOVIE CHANGED] Movie images updated, all data repulled from Guidebox, movie ID: {$mid} +++");

	}
	/**
	* Get Show ID from Episode ID
	*/
	public function episodeIDtoShowID($episode_id){
		$show_id=$this->db->query("SELECT show_id FROM episodes WHERE id=".$episode_id)->result_array()[0]['show_id'];
		return $show_id;
	}
	/**
	* Add the show_id value to all existing sources
	*/
	public function addShowID(){
		$episodes_db = [
				'episodes_sources_free_android',
				'episodes_sources_free_ios',
				'episodes_sources_free_web',
				'episodes_sources_purchase_android',
				'episodes_sources_purchase_ios',
				'episodes_sources_purchase_web',
				'episodes_sources_subscription_android',
				'episodes_sources_subscription_ios',
				'episodes_sources_subscription_web',
				'episodes_sources_tv_everywhere_android',
				'episodes_sources_tv_everywhere_ios',
				'episodes_sources_tv_everywhere_web'
			];
		foreach($episodes_db as $typeOfSource){
			$sources = $this->db->query("SELECT * FROM {$typeOfSource}")->result_array();
			foreach ($sources as $source){
				//get episode id
				$ep_id = $source['episode_id'];
				//get show id for this episode
				$show_id=$this->db->query("SELECT show_id FROM episodes WHERE id=".$ep_id)->result_array()[0]['show_id'];
				//insert into sources
				$this->db->query("UPDATE {$typeOfSource} SET show_id={$show_id} WHERE episode_id={$ep_id}");
			}
		}
	}
    /**
     * Sync DB with Guidebox API
     * @throws ErrorException
     */
    public function sync()
    {

        /*
         * 1. Check if response contains data
         * 2. Delete and pull again the same ids
         */

        /* --------------------------------------------------------------------------------------------------------
         * MOVIES
         * --------------------------------------------------------------------------------------------------------
         */

        // MOVIES CHANGED
        $movie_changes = $this->_curl($this->url . "updates/movies/changes/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [CHANGED MOVIES] Checking... ------------------------------ ");
        if (!empty($movie_changes) || !empty($movie_changes['results'])) {
            for ($i = count($movie_changes['results']) - 1; $i >= 0; $i--) {

                if (!empty($this->Db->select('movies', $movie_changes['results'][$i]['id']))) {
                    $status = $this->deleteMovie($movie_changes['results'][$i]['id']);
                    log_message('info', "[MOVIE CHANGED] Movie deleted, movie ID: {$movie_changes['results'][$i]['id']} +++");

                    $this->processMovieById($movie_changes['results'][$i]['id']);
                    log_message('info', "[MOVIE CHANGED] Movie data updated, all data repulled from Guidebox, movie ID: {$movie_changes['results'][$i]['id']} +++");

					$this->processMovieTrailers($movie_changes['results'][$i]['id']);
                    log_message('info', "[MOVIE CHANGED] Movie trailers updated, all data repulled from Guidebox, movie ID: {$movie_changes['results'][$i]['id']} +++");

					$this->processMovieImages($movie_changes['results'][$i]['id']);
					log_message('info', "[MOVIE CHANGED] Movie images updated, all data repulled from Guidebox, movie ID: {$movie_changes['results'][$i]['id']} +++");

                    // If any movie that exists in our DB gets changed, it will be logged
                    if ($status) {
                        $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('movie_changed', '{$this->new_timestamp}', '{$movie_changes['results'][$i]['id']}')");
                    }
                } else {
                    log_message('info', "Changed movie ID: {$movie_changes['results'][$i]['id']} not found in our local DB.");
                }
            }
        }

		//wait 1 second
        usleep(1000000);

        // MOVIES DELETED
        $movie_deleted = $this->_curl($this->url . "updates/movies/deletes/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [DELETED MOVIES] Checking... ------------------------------ ");
        if (!empty($movie_deleted) || !empty($movie_deleted['results'])) {
            for ($i = count($movie_deleted['results']) - 1; $i >= 0; $i--) {

                // Check if we have this deleted movie id in our DB
                if (!empty($this->Db->select('movies', $movie_deleted['results'][$i]['id']))) {
                    $status = $this->deleteMovie($movie_deleted['results'][$i]['id']);
                    log_message('info', "[MOVIE DELETED] Movie was removed from Guidebox db, thus we'll remove it from our db as well, movie ID: {$movie_deleted['results'][$i]['id']}");

                    // If any movie that exists in our DB gets changed, it will be logged
                    if ($status) {
                        $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('movie_deleted', '{$this->new_timestamp}', '{$movie_deleted['results'][$i]['id']}')");
                    }

                } else {
                    log_message('error', "Deleted movie ID: {$movie_deleted['results'][$i]['id']} was not found in our local DB.");
                }
            }
        }

        //wait 1 second
        usleep(1000000);

        /* --------------------------------------------------------------------------------------------------------
         * SHOWS
         * --------------------------------------------------------------------------------------------------------
         */

        // CHANGED SHOWS
        $show_changes = $this->_curl($this->url . "/updates/shows/changes/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [CHANGED SHOWS] Checking... ------------------------------ ");
        if (!empty($show_changes) || !empty($show_changes['results'])) {
            for ($i = count($show_changes['results']) - 1; $i >= 0; $i--) {

                if (!empty($this->Db->select('shows', $show_changes['results'][$i]['id']))) {
                    $status = $this->deleteShow($show_changes['results'][$i]['id']);
                    log_message('info', "[CHANGED SHOWS] Deleted changed show ID: {$show_changes['results'][$i]['id']} +++");

                    $this->processShowById($show_changes['results'][$i]['id']);
                    log_message('info', "[CHANGED SHOWS] Processed changed show ID: {$show_changes['results'][$i]['id']} +++");

                    // If any show that exists in our DB gets changed, it will be logged
                    if ($status) {
                        $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('show_changed', '{$this->new_timestamp}', '{$show_changes['results'][$i]['id']}')");
                    }
                } else {
                    log_message('info', "[CHANGED SHOWS] Changed show ID: {$show_changes['results'][$i]['id']} was not found in our local DB.");
                }
            }
        }

        //wait 1 second
        usleep(1000000);

        // DELETED SHOWS
        $deleted_shows = $this->_curl($this->url . "/updates/shows/deletes/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [DELETED SHOWS] Checking... ------------------------------ ");
        if (!empty($deleted_shows) || !empty($deleted_shows['results'])) {
            for ($i = count($deleted_shows['results']) - 1; $i >= 0; $i--) {

                if (!empty($this->Db->select('shows', $deleted_shows['results'][$i]['id']))) {

                    $status = $this->deleteShow($deleted_shows['results'][$i]['id']);
                    log_message('info', "[DELETED SHOWS] Deleted show ID: {$deleted_shows['results'][$i]['id']}");

                    if ($status) {
                        $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('shows_deleted', '{$this->new_timestamp}', '{$deleted_shows['results'][$i]['id']}')");
                    }
                } else {
                    log_message('info', "[DELETED SHOWS] Deleted show ID: {$deleted_shows['results'][$i]['id']} was not found in our local DB.");
                }
            }
        }

        //wait 1 second
        usleep(1000000);

        // Shows - NEW EPISODES - Show IDs are listed here when they have new episodes added to Guidebox.
        $new_show_episodes = $this->_curl($this->url . "/updates/shows/new_episodes/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [Shows - NEW EPISODES] Checking... ------------------------------ ");
        if (!empty($new_show_episodes) || !empty($new_show_episodes['results'])) {
            for ($i = count($new_show_episodes['results']) - 1; $i >= 0; $i--) {

                $this->processShowEpisodes($new_show_episodes['results'][$i]['id']);
                log_message('info', "[Shows - NEW EPISODES] Processed new episode for Show ID: {$new_show_episodes['results'][$i]['id']} +++");

            }
        }

		//wait 1 second
        usleep(1000000);


        /* --------------------------------------------------------------------------------------------------------
         * EPISODES
         * --------------------------------------------------------------------------------------------------------
         */


        // NEW EPISODES
        $new_episodes = $this->_curl($this->url . "/updates/episodes/new/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [NEW EPISODES] Checking... ------------------------------ ");
        if (!empty($new_episodes) || !empty($new_episodes['results'])) {
            for ($i = count($new_episodes['results']) - 1; $i >= 0; $i--) {

                if (empty($this->Db->select('episodes', $new_episodes['results'][$i]['id']))) {

                    $this->processEpisode($new_episodes['results'][$i]['id']);
                    log_message('info', "[NEW EPISODES] Processed new episode ID: {$new_episodes['results'][$i]['id']} +++");

                    if ($status) {
                        $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('new_episode', '{$this->new_timestamp}', '{$new_episodes['results'][$i]['id']}')");
                    }
                } else {
                    log_message('info', "[NEW EPISODES] New episode ID: {$new_episodes['results'][$i]['id']} already exists in local DB.");
                }
            }
        }

		//wait 1 second
        usleep(1000000);

        // CHANGED EPISODES
        $changed_episodes = $this->_curl($this->url . "/updates/episodes/changes/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [CHANGED EPISODES] Checking... ------------------------------ ");
        if (!empty($changed_episodes) || !empty($changed_episodes['results'])) {
            for ($i = count($changed_episodes['results']) - 1; $i >= 0; $i--) {

                if (!empty($this->Db->select('episodes', $changed_episodes['results'][$i]['id']))) {
                    $status = $this->deleteEpisode($changed_episodes['results'][$i]['id']);
                    log_message('info', "[CHANGED EPISODES] Changed episode ID: {$changed_episodes['results'][$i]['id']}");

                    $this->processEpisode($changed_episodes['results'][$i]['id']);
                    log_message('info', "[CHANGED EPISODES] Processed changed episode ID: {$changed_episodes['results'][$i]['id']} +++");

                    if ($status) {
                        $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('episode_changed', '{$this->new_timestamp}', '{$changed_episodes['results'][$i]['id']}')");
                    }
                } else {
                    log_message('info', "[CHANGED EPISODES] Changed episode ID: {$changed_episodes['results'][$i]['id']} not found in local DB.");
                }
            }
        }

		//wait 1 second
        usleep(1000000);


        // DELETED EPISODES
        $deleted_episodes = $this->_curl($this->url . "/updates/episodes/deletes/{$this->last_timestamp}?limit=1000&page=1");
        log_message('info', " ------------------------------ [DELETED EPISODES] Checking... ------------------------------ ");
        if (!empty($deleted_episodes) || !empty($deleted_episodes['results'])) {
            for ($i = count($deleted_episodes['results']) - 1; $i >= 0; $i--) {

                $status = $this->deleteEpisode($deleted_episodes['results'][$i]['id']);

                if ($status) {
                    $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('episode_deleted', '{$this->new_timestamp}', '{$deleted_episodes['results'][$i]['id']}')");
                    log_message('info', "[DELETED EPISODES] Deleted episode ID: {$deleted_episodes['results'][$i]['id']}");
                }
            }
        }

        usleep(1000000);
        //set guidebox "rating" position
        $this->updateGuideboxRating();


        // Mark the last sync
        $this->setNewTimestamp();
    }
    public function sync_update(){
      /*
      * Updated version of the old sync method. We will check each and update each information instead of deleting and repulling all the information
      */

      //Guidebox API - Movies changed
      // $mock = json_decode('{"total_results":1,"total_returned":1,"page":1,"total_pages":1,"results":[{"id":49876,"time":1458739990}],"development_api_key":"You are currently using a temporary development API key.  This API key is for testing only.  You have used 118 of 250 available calls.  Please see the API docs (http:\/\/api.guidebox.com\/apidocs) for additional information or to generate a production API key."}',true);
      //$mock = json_decode('{"total_results":22,"total_returned":22,"page":1,"total_pages":1,"results":[{"id":5701,"time":1458739990},{"id":54706,"time":1458740362},{"id":5484,"time":1458740512},{"id":5556,"time":1458740516},{"id":10205,"time":1458740552},{"id":5866,"time":1458740556},{"id":10576,"time":1458740557},{"id":21418,"time":1458740592},{"id":27643,"time":1458740617},{"id":41856,"time":1458740653},{"id":52766,"time":1458740699},{"id":65570,"time":1458740737},{"id":68709,"time":1458740741},{"id":79715,"time":1458740760},{"id":83074,"time":1458740776},{"id":83318,"time":1458740780},{"id":6709,"time":1458740941},{"id":8004,"time":1458741319},{"id":5823,"time":1458741611},{"id":104675,"time":1458741756},{"id":94528,"time":1458741881},{"id":73029,"time":1458741984}],"development_api_key":"You are currently using a temporary development API key.  This API key is for testing only.  You have used 120 of 250 available calls.  Please see the API docs (http:\/\/api.guidebox.com\/apidocs) for additional information or to generate a production API key."}',true);

      $movie_changes = $this->_curl($this->url . "updates/movies/changes/{$this->last_timestamp}?limit=1000&page=1");
      log_message('info',"------------------------------ [CHANGED MOVIES] Checking... ------------------------------ ");
      if(!empty($movie_changes['results']) || !empty($movie_changes)){
        for($i = 0; $i<count($movie_changes['results']);$i++){
          $changed_movie_id = $movie_changes['results'][$i]['id'];
          //we check if this movie id is in our database
          if(!empty($this->Db->select('movies',$changed_movie_id))){
              log_message('info',"[MOVIE CHANGED] Movie ID {$changed_movie_id} has changed. Updating information");
              $this->updateMovieById($changed_movie_id);
              $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('movie_changed', '{$this->new_timestamp}', '{$movie_changes['results'][$i]['id']}')");

          } else {
            log_message('info',"Movie ID {$changed_movie_id} not in our database, skipping");
          }
          usleep(3000000);
        }
      }
      usleep(3000000);
      //Guidebox API - Shows changed
      //$mock = json_decode('{"total_results":24,"total_returned":24,"page":1,"total_pages":1,"results":[{"id":56,"time":1458745872}]}',true);
      $shows_changes = $this->_curl($this->url."updates/shows/changes/{$this->last_timestamp}?limit=1000&page=1");
      log_message('info',"------------------------------ [CHANGED SHOWS] Checking... ------------------------------ ");
      if(!empty($shows_changes['results']) || !empty($shows_changes)){
        for($i = 0; $i<count($shows_changes['results']);$i++){
          $changed_show_id = $shows_changes['results'][$i]['id'];
          //we check if this movie id is in our database
          if(!empty($this->Db->select('shows',$changed_show_id))){
              log_message('info',"[SHOW CHANGED] Show ID {$changed_show_id} has changed. Updating information");
              $this->updateShowById($changed_show_id);
              $this->db->query("insert into `updates_all` (`type`, `last_timestamp`, `guidebox_id`) values ('show_changed', '{$this->new_timestamp}', '{$shows_changes['results'][$i]['id']}')");

          } else {
            log_message('info',"Show ID {$changed_show_id} not in our database, skipping");
          }
          usleep(3000000);
        }
      }
    }
    public function test_updateShowById($show_id){
      $url = $this->url . "episode/{$show_id}";
      $req = $this->_curl($url);
      //Safety net
      if (empty($req)) {
          log_message('error', "/shows/id - RECEIVED EMPTY RESULTS. Stopped at movie id {$id}.");
          die();
      }
      $sources_tables = array("free_android_sources"=>"episodes_sources_free_android","free_ios_sources"=>"episodes_sources_free_ios","free_web_sources"=>"episodes_sources_free_web","purchase_android_sources"=>"episodes_sources_purchase_android","purchase_ios_sources"=>"episodes_sources_purchase_ios","purchase_web_sources"=>"episodes_sources_purchase_web","subscription_android_sources"=>"episodes_sources_subscription_android","subscription_ios_sources"=>"episodes_sources_subscription_ios","subscription_web_sources"=>"episodes_sources_subscription_web","tv_everywhere_web_sources"=>"episodes_sources_tv_everywhere_web","tv_everywhere_ios_sources"=>"episodes_sources_tv_everywhere_ios","tv_everywhere_android_sources"=>"episodes_sources_tv_everywhere_android");
      //verific fiecare tabel de surse pentru acest episod
      foreach($sources_tables as $key=>$source){
        //pentru $source iau toate sursele de la noi din tabel
        $db_res = $this->db->query("SELECT * FROM {$source} WHERE episode_id={$show_id}")->result_array();


        //stochez sursele din raspunul guidebox pentru acest tip de $source
        $available_sources = array();
        foreach($req[$key] as $a_src){
          $available_sources[] = $a_src['id'];
        }

        //verific fiecare sursa din $source daca exista in raspunsul guidebox
        foreach($db_res as $res){
          if(in_array($res['id'],$available_sources)){
            log_message("info","{$key} - {$res['id']} inseamna ca sursa verificata este in baza de date si in raspunsul guidebox");
            echo $res['id']. " ok";
          } else {
            log_message("info","{$key} - {$res['id']} in our database but not in guidebox's response. deleting...");
            echo $res['id']. " not available";
          }
        }
      }


    }
    public function updateShowById($show_id){
      $url = $this->url . 'shows/' . $show_id;
      $req = $this->_curl($url);
      // Safety net
      if (empty($req)) {
          log_message('error', "/shows/id - RECEIVED EMPTY RESULTS. Stopped at movie id {$id}.");
          die();
      }

      $data = [];
      $table_columns = $this->db->query("DESCRIBE shows")->result_array();
      foreach($table_columns as $index=>$val){
        if(array_key_exists($val['Field'],$req)){
          if(is_array($req[$val['Field']]) || is_object($req[$val['Field']])){
            $data[$val['Field']]=json_encode($req[$val['Field']]);
          } else{
            $data[$val['Field']]=$req[$val['Field']];
          }
        }
      }

      $original_data = $this->db->get_where("shows",array('id'=>$show_id))->result_array();
      foreach($original_data[0] as $index => $checkedField){
        //we iterate through each item in the result and compare it with the one from our database
        if(array_key_exists($index,$data)){
          //log_message("info","[MOVIE UPDATE] Check if {$index} column has changed");
          if($checkedField !=$data[$index]){
            log_message("info","Field {$index} has changed ({$checkedField} => {$data[$index]})");
            $this->db->query("UPDATE shows SET `{$index}`=".$this->db->escape($data[$index])." WHERE id={$show_id}");
          } else {
            //log_message("info","Field {$index} has not changed");
          }
        }
      }

      //shows_to_cast
      if(!empty($req['cast']) && $req['cast']!='[]'){
        $shows_to_cast_original = $this->db->get_where('shows_to_cast',array("show_id"=>$show_id))->result_array();

        //we store the current person ids in an array
        $original_cast_for_this_show = array();
        foreach($shows_to_cast_original as $show_cast){
          $original_cast_for_this_show[]=$show_cast['person_id'];
        }

        //we store the guidebox returned person ids in an array
        $new_cast_for_this_show = array();
        $new_cast_character_shows = array();
        foreach($req['cast'] as $show_cast){
          $new_cast_for_this_show[]=$show_cast['id'];
          $new_cast_character_names[$show_cast['id']] =$show_cast['character_name'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_cast_for_this_show as $cast_id){
          if(in_array($cast_id,$original_cast_for_this_show)){
            //this actor id is already assigned to this show
            // log_message("info","[SHOW CHANGED] cast ID:{$cast_id} already in db");
          } else {
            $character_name = $new_cast_character_names[$cast_id];
            //this actor id is new and must be added to the database
            $this->db->query("insert into shows_to_cast(show_id, person_id,character_name) values ({$this->db->escape($show_id)},{$this->db->escape($cast_id)},{$this->db->escape($character_name)})");
            log_message('info',"[SHOW CHANGED] New cast for ID:{$show_id} -> Cast ID: {$cast_id}");
          }
        }
      }

      //shows_to_tags
      if(!empty($req['tags']) && $req['tags']!='[]'){
        $shows_to_tags_original = $this->db->get_where('shows_to_tags',array("show_id"=>$show_id))->result_array();

        //we store the current tag ids in an array
        $original_tags_for_this_show = array();
        foreach($shows_to_tags_original as $show_tag){
          $original_tags_for_this_show[]=$show_tag['tag_id'];
        }

        //we store the guidebox returned tag ids in an array
        $new_tags_for_this_show = array();
        foreach($req['tags'] as $show_tag){
          $new_tags_for_this_show[]=$show_tag['id'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_tags_for_this_show as $tag_id){
          if(in_array($tag_id,$original_tags_for_this_show)){
            //this tag id is already assigned to this show
          } else {
            //this tag id is new and must be added to the database
            $this->db->query("insert into shows_to_tags(show_id, tag_id) values ({$this->db->escape($show_id)},{$this->db->escape($tag_id)})");
            log_message('info',"[show CHANGED] New tag for ID:{$show_id} -> Tag ID: {$tag_id}");
          }
        }
      }

      //shows_to_genres
      if(!empty($req['genres']) && $req['genres']!='[]'){
        $shows_to_genres_original = $this->db->get_where('shows_to_genres',array("show_id"=>$show_id))->result_array();

        //we store the current genre ids in an array
        $original_genres_for_this_show = array();
        foreach($shows_to_genres_original as $show_genre){
          $original_genres_for_this_show[]=$show_genre['genre_id'];
        }

        //we store the guidebox returned genre ids in an array
        $new_genres_for_this_show = array();
        foreach($req['genres'] as $show_genre){
          $new_genres_for_this_show[]=$show_genre['id'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_genres_for_this_show as $gen_id){
          if(in_array($gen_id,$original_genres_for_this_show)){
            //this genre id is already assigned to this show
          } else {
            //this genre id is new and must be added to the database
            $this->db->query("insert into shows_to_genres(show_id, genre_id) values ({$this->db->escape($show_id)},{$this->db->escape($gen_id)})");
            log_message('info',"[show CHANGED] New genre for ID:{$show_id} -> Genre ID: {$gen_id}");
          }
          echo "<br>";
        }
      }

      //shows_to_channels
      if(!empty($req['channels']) && $req['channels']!='[]'){
        $shows_to_channels_original = $this->db->get_where('shows_to_channels',array("show_id"=>$show_id))->result_array();

        //we store the current channel ids in an array
        $original_channels_for_this_show = array();
        foreach($shows_to_channels_original as $show_channel){
          $original_channels_for_this_show[]=$show_channel['channel_id'];
        }

        //we store the guidebox returned channel ids in an array
        $new_channels_for_this_show = array();
        foreach($req['channels'] as $show_channel){
          $new_channels_for_this_show[]=$show_channel['id'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_channels_for_this_show as $gen_id){
          if(in_array($gen_id,$original_channels_for_this_show)){
            //this channel id is already assigned to this show
          } else {
            //this channel id is new and must be added to the database
            $this->db->query("insert into shows_to_channels(show_id, channel_id) values ({$this->db->escape($show_id)},{$this->db->escape($gen_id)})");
            log_message('info',"[show CHANGED] New channel for ID:{$show_id} -> channel ID: {$gen_id}");
          }
          echo "<br>";
        }
      }
      $this->updateShowToSeasons($show_id);
      $this->updateShowToEpisodes($show_id);
      //update available sources
      $this->updateAllSources($show_id,"show");
    }
    public function updateShowToSeasons($show_id){
      $url = $this->url . "show/{$show_id}/seasons";
      usleep(500000);

      $req = $this->_curl($url)['results'];

      $data = [];
      $original_data_seasons = $this->db->get_where("shows_to_seasons",array("show_id"=>$show_id))->result_array();
      if(count($original_data_seasons) < count($req)){
        log_message("info","[SHOW CHANGED] New season added for show ID: {$show_id}");
        $season_number = $req[count($req)-1]['season_number'];
        $first_airdate = $req[count($req)-1]['first_airdate'];
        $this->db->query("insert into shows_to_seasons(show_id,season_number,first_airdate) values ({$this->db->escape($show_id)},{$this->db->escape($season_number)},{$this->db->escape($first_airdate)})");
      }
    }

    public function updateShowToEpisodes($show_id){
      $seasons = $this->db->get_where("shows_to_seasons",array("show_id"=>$show_id))->result_array();

      //store already grabbed episodes in this variable
      // $original_episodes = $this->db->get_where("episodes",array("show_id"=>$show_id))->result_array();

      foreach($seasons as $season){
        $season_number=$season['season_number'];
        log_message("info","Checking season {$season_number}");
        $url = $this->url."show/{$show_id}/episodes/{$season_number}/0/100";
        $req = $this->_curl($url);

        if(!empty($req) || empty($req['results'])){
            foreach($req['results'] as $episode){
              //check if this episode is already in db
              $episode_check = $this->db->get_where("episodes",array('id'=>$episode['id'],'show_id'=>$show_id))->result_array();
              if(empty($episode_check)){
                log_message("info","[SHOW CHANGE] New episode for show ID {$show_id} -> episode ID {$episode['id']}");
                $this->processEpisode($episode['id']);
              } else {
                // episode already in db
                // log_message("info","Episode {$episode['id']} already in db");
              }
              log_message("info","Checking sources...");
              usleep(100000);

              $url = $this->url . "episode/{$episode['id']}";
              $req = $this->_curl($url);


              $sources_tables = array("free_android_sources"=>"episodes_sources_free_android","free_ios_sources"=>"episodes_sources_free_ios","free_web_sources"=>"episodes_sources_free_web","purchase_android_sources"=>"episodes_sources_purchase_android","purchase_ios_sources"=>"episodes_sources_purchase_ios","purchase_web_sources"=>"episodes_sources_purchase_web","subscription_android_sources"=>"episodes_sources_subscription_android","subscription_ios_sources"=>"episodes_sources_subscription_ios","subscription_web_sources"=>"episodes_sources_subscription_web","tv_everywhere_web_sources"=>"episodes_sources_tv_everywhere_web","tv_everywhere_ios_sources"=>"episodes_sources_tv_everywhere_ios","tv_everywhere_android_sources"=>"episodes_sources_tv_everywhere_android");
              //$key e numele raspunsului de la gudebox $source e numele tabelului din db-ul nostru
              foreach($sources_tables as $key=>$source){
                //pentru $source iau toate sursele de la noi din tabel

                $db_res = $this->db->query("SELECT * FROM {$source} WHERE episode_id={$episode['id']}")->result_array();


                //stochez sursele din raspunul guidebox pentru acest tip de $source
                $available_sources = array();
                foreach($req[$key] as $a_src){
                  $available_sources[] = $a_src['id'];
                }

                //verific fiecare sursa din $source daca exista in raspunsul guidebox
                foreach($db_res as $res){
                  if(in_array($res['id'],$available_sources)){
                    //log_message("info","{$key} - {$res['id']} inseamna ca sursa verificata este in baza de date si in raspunsul guidebox");
                  } else {
                    log_message("info","{$key} - {$res['id']} in our database but not in guidebox's response. deleting...");
                    $this->db->query("DELETE FROM {$source} WHERE id={$res['id']}");
                  }
                  }
              }

            }
        }
        log_message("info","Wait a sec");
        usleep(100000);

      }

    }
    public function updateAllSources($id,$unit){
      //id este id-ul din tabel
      //unit poate fi: movie, show, season. fiecare va fi tratat separat
      if($unit=="movie"){
        $sources_tables = ['movies_sources_free_android','movies_sources_free_ios','movies_sources_free_web','movies_sources_other','movies_sources_purchase_android','movies_sources_purchase_ios','movies_sources_purchase_web','movies_sources_subscription_android','movies_sources_subscription_ios','movies_sources_subscription_web','movies_sources_tv_everywhere_android','movies_sources_tv_everywhere_ios','movies_sources_tv_everywhere_web'];
        $available_sources = [];
        foreach($sources_tables as $source_table){
          $res = $this->db->query("SELECT * FROM {$source_table} WHERE movie_id={$id}")->result_array();
          if(count($res)>0){
              foreach($res as $source){
                $source_name = $source['source'];
                if(!in_array($source_name,$available_sources)){
                  $available_sources[] = $source_name;
                }
              }
          }
        }
        //print_r(json_encode($available_sources));
        $available_sources_encode = json_encode($available_sources);
        $this->db->update("movies",array("available_sources"=>json_encode($available_sources)),"id=".$id);
      }elseif($unit=="show"){
        $sources_tables = ['episodes_sources_free_android','episodes_sources_free_ios','episodes_sources_free_web','episodes_sources_purchase_android','episodes_sources_purchase_ios','episodes_sources_purchase_web','episodes_sources_subscription_android','episodes_sources_subscription_ios','episodes_sources_subscription_web','episodes_sources_tv_everywhere_android','episodes_sources_tv_everywhere_ios','episodes_sources_tv_everywhere_web'];
        $available_sources = [];
        foreach($sources_tables as $source_table){
          $res = $this->db->query("SELECT * FROM {$source_table} WHERE show_id={$id}")->result_array();
          if(count($res)>0){
              foreach($res as $source){
                    $source_name = $source['source'];
                    if(!in_array($source_name,$available_sources)){
                      $available_sources[] = $source_name;
                    }
              }
          }
        }
        $available_sources_encode = json_encode($available_sources);
        $this->db->update("shows",array("available_sources"=>json_encode($available_sources)),"id=".$id);
      }
    }
    public function updateMovieById($movie_id){

      $url = $this->url.'movie/'.$movie_id;

      $req = $this->_curl($url);
      // Safety net
      if (empty($req)) {
          log_message('error', "/movies/id - RECEIVED EMPTY RESULTS. Stopped at movie id {$id}.");
          die();
      }

      $data = [];
      $table_columns = $this->db->query("DESCRIBE movies")->result_array();
      foreach($table_columns as $index=>$val){
        if(array_key_exists($val['Field'],$req)){
          if(is_array($req[$val['Field']]) || is_object($req[$val['Field']])){
            $data[$val['Field']]=json_encode($req[$val['Field']]);
          } else{
            $data[$val['Field']]=$req[$val['Field']];
          }
        }
      }

      //$data holds the information to be stored in the movies table

      //updating the `movies` table
      $original_data = $this->db->get_where("movies",array('id'=>$movie_id))->result_array();
      foreach($original_data[0] as $index => $checkedField){
        //we iterate through each item in the result and compare it with the one from our database
        if(array_key_exists($index,$data)){
          //log_message("info","[MOVIE UPDATE] Check if {$index} column has changed");
          if($checkedField !=$data[$index]){
            log_message("info","Field {$index} has changed ({$checkedField} => {$data[$index]})");
            $this->db->query("UPDATE movies SET `{$index}`=".$this->db->escape($data[$index])." WHERE id={$movie_id}");
          } else {
            //log_message("info","Field {$index} has not changed");
          }
        }
      }

      /* updating the `movies_to_genres` table
      The movies_to_genres table stores the movie_id and the genre_id.
      A movie could have more genres, so there are multiple entries for the same movie_id
      */
      if(!empty($req['genres']) && $req['genres']!='[]'){
        $movies_to_genres_original = $this->db->get_where('movies_to_genres',array("movie_id"=>$movie_id))->result_array();

        //we store the current genre ids in an array
        $original_genres_for_this_movie = array();
        foreach($movies_to_genres_original as $movie_genre){
          $original_genres_for_this_movie[]=$movie_genre['genre_id'];
        }

        //we store the guidebox returned genre ids in an array
        $new_genres_for_this_movie = array();
        foreach($req['genres'] as $movie_genre){
          $new_genres_for_this_movie[]=$movie_genre['id'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_genres_for_this_movie as $gen_id){
          if(in_array($gen_id,$original_genres_for_this_movie)){
            //this genre id is already assigned to this movie
          } else {
            //this genre id is new and must be added to the database
            $this->db->query("insert into movies_to_genres(movie_id, genre_id) values ({$this->db->escape($movie_id)},{$this->db->escape($gen_id)})");
            log_message('info',"[MOVIE CHANGED] New genre for ID:{$movie_id} -> Genre ID: {$gen_id}");
          }
          echo "<br>";
        }
      }

      /* updating the `movies_to_tags` table
      The movies_to_tags table stores the movie_id and the tag_id.
      A movie could have more tags, so there are multiple entries for the same movie_id
      */
      if(!empty($req['tags']) && $req['tags']!='[]'){
        $movies_to_tags_original = $this->db->get_where('movies_to_tags',array("movie_id"=>$movie_id))->result_array();

        //we store the current tag ids in an array
        $original_tags_for_this_movie = array();
        foreach($movies_to_tags_original as $movie_tag){
          $original_tags_for_this_movie[]=$movie_tag['tag_id'];
        }

        //we store the guidebox returned tag ids in an array
        $new_tags_for_this_movie = array();
        foreach($req['tags'] as $movie_tag){
          $new_tags_for_this_movie[]=$movie_tag['id'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_tags_for_this_movie as $tag_id){
          if(in_array($tag_id,$original_tags_for_this_movie)){
            //this tag id is already assigned to this movie
          } else {
            //this tag id is new and must be added to the database
            $this->db->query("insert into movies_to_tags(movie_id, tag_id) values ({$this->db->escape($movie_id)},{$this->db->escape($tag_id)})");
            log_message('info',"[MOVIE CHANGED] New tag for ID:{$movie_id} -> Tag ID: {$tag_id}");
          }
        }
      }

      /* updating the `movies_to_writers` table
      The movies_to_writers table stores the movie_id and the person_id.
      A movie could have more writers, so there are multiple entries for the same movie_id
      */
      if(!empty($req['writers']) && $req['writers']!='[]'){
        $movies_to_writers_original = $this->db->get_where('movies_to_writers',array("movie_id"=>$movie_id))->result_array();

        //we store the current person ids in an array
        $original_writers_for_this_movie = array();
        foreach($movies_to_writers_original as $movie_writer){
          $original_writers_for_this_movie[]=$movie_writer['person_id'];
        }

        //we store the guidebox returned tag ids in an array
        $new_writers_for_this_movie = array();
        foreach($req['writers'] as $movie_writer){
          $new_writers_for_this_movie[]=$movie_writer['id'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_writers_for_this_movie as $writer_id){
          if(in_array($writer_id,$original_writers_for_this_movie)){
            //this writer id is already assigned to this movie
            //log_message("info","[MOVIE CHANGED] Writer ID:{$writer_id} already in db");
          } else {
            //this writer id is new and must be added to the database
            $this->db->query("insert into movies_to_writers(movie_id, person_id) values ({$this->db->escape($movie_id)},{$this->db->escape($writer_id)})");
            log_message('info',"[MOVIE CHANGED] New writer for ID:{$movie_id} -> Tag ID: {$writer_id}");
          }
        }
      }

      /* updating the `movies_to_directors` table
      The movies_to_writers table stores the movie_id and the person_id.
      A movie could have more writers, so there are multiple entries for the same movie_id
      */
      if(!empty($req['directors']) && $req['directors']!='[]'){
        $movies_to_directors_original = $this->db->get_where('movies_to_directors',array("movie_id"=>$movie_id))->result_array();

        //we store the current person ids in an array
        $original_directors_for_this_movie = array();
        foreach($movies_to_directors_original as $movie_director){
          $original_directors_for_this_movie[]=$movie_director['person_id'];
        }

        //we store the guidebox returned person ids in an array
        $new_directors_for_this_movie = array();
        foreach($req['directors'] as $movie_director){
          $new_directors_for_this_movie[]=$movie_director['id'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_directors_for_this_movie as $director_id){
          if(in_array($director_id,$original_directors_for_this_movie)){
            //this writer id is already assigned to this movie
            //log_message("info","[MOVIE CHANGED] director ID:{$director_id} already in db");
          } else {
            //this writer id is new and must be added to the database
            $this->db->query("insert into movies_to_directors(movie_id, person_id) values ({$this->db->escape($movie_id)},{$this->db->escape($director_id)})");
            log_message('info',"[MOVIE CHANGED] New director for ID:{$movie_id} -> Tag ID: {$director_id}");
          }
        }
      }

      /* updating the `movies_to_cast` table
      The movies_to_cast table stores the movie_id, the person_id and the character_name.
      A movie might have more than one actor, so there are multiple entries for the same movie_id
      */
      if(!empty($req['cast']) && $req['cast']!='[]'){
        $movies_to_cast_original = $this->db->get_where('movies_to_cast',array("movie_id"=>$movie_id))->result_array();

        //we store the current person ids in an array
        $original_cast_for_this_movie = array();
        foreach($movies_to_cast_original as $movie_cast){
          $original_cast_for_this_movie[]=$movie_cast['person_id'];
        }

        //we store the guidebox returned person ids in an array
        $new_cast_for_this_movie = array();
        $new_cast_character_names = array();
        foreach($req['cast'] as $movie_cast){
          $new_cast_for_this_movie[]=$movie_cast['id'];
          $new_cast_character_names[$movie_cast['id']] =$movie_cast['character_name'];
        }

        //check if there are any new values in guidebox's response
        foreach($new_cast_for_this_movie as $cast_id){
          if(in_array($cast_id,$original_cast_for_this_movie)){
            //this actor id is already assigned to this movie
            // log_message("info","[MOVIE CHANGED] cast ID:{$cast_id} already in db");
          } else {
            $character_name = $new_cast_character_names[$cast_id];
            //this actor id is new and must be added to the database
            $this->db->query("insert into movies_to_cast(movie_id, person_id,character_name) values ({$this->db->escape($movie_id)},{$this->db->escape($cast_id)},{$this->db->escape($character_name)})");
            log_message('info',"[MOVIE CHANGED] New cast for ID:{$movie_id} -> Tag ID: {$cast_id}");
          }
        }
      }

      /* updating sources */
      //free_web_sources
      $resource_name = "free_web_sources";
      $table_name = "movies_sources_free_web";
      // log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          // log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        // log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //tv_everywhere_web_sources
      $resource_name = "tv_everywhere_web_sources";
      $table_name = "movies_sources_tv_everywhere_web";
      // log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }

          }
        } else {
          // log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        // log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //subscription_web_sources
      $resource_name = "subscription_web_sources";
      $table_name = "movies_sources_subscription_web";
      // log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          // log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        // log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //purchase_web_sources
      $resource_name = "purchase_web_sources";
      $table_name = "movies_sources_purchase_web";
      // log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];

        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"link"=>$new_source['link'],"formats"=>json_encode($new_source['formats'])));
            }
          }
        } else {
          //log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        //log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //other_sources
      // $resource_name = "other_sources";
      // $table_name = "movies_sources_other";
      // log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      // if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
      //   $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
      //   $new_sources = $req[$resource_name];
      //   //format new source response for easier comparison
      //   foreach($new_sources as $index=>$source){
      //     $new_sources[$index]['formats']="null";
      //     $new_sources[$index]['movie_id']=$movie_id;
      //   }
      //   if(count($new_sources)!=count($original_sources)){
      //     log_message("info","[MOVIE CHANGED] {$resource_name} changed");
      //     foreach($new_sources as $new_source){
      //       if(in_array($new_source,$original_sources)){
      //         //source already saved, skipping
      //         log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
      //       } else {
      //         $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"link"=>$new_source['link'],"formats"=>"null"));
      //         log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
      //       }
      //     }
      //   } else {
      //     log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
      //   }
      // }

      //purchase_ios_sources
      $resource_name = "purchase_ios_sources";
      $table_name = "movies_sources_purchase_ios";
      // log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['tv_channel']="NULL";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>json_encode($new_source['formats'])));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          //log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        //log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //purchase_android_sources
      $resource_name = "purchase_android_sources";
      $table_name = "movies_sources_purchase_android";
      //log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['tv_channel']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>json_encode($new_source['formats'])));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          //log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        //log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //free_android_sources
      $resource_name = "free_android_sources";
      $table_name = "movies_sources_free_android";
      //log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['tv_channel']="null";
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          //log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        //log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //free_ios_sources
      $resource_name = "free_ios_sources";
      $table_name = "movies_sources_free_ios";
      //log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['tv_channel']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          //log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        //log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //subscription_ios_sources
      $resource_name = "subscription_ios_sources";
      $table_name = "movies_sources_subscription_ios";
      // log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['tv_channel']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          //log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        //log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //subscription_android_sources
      $resource_name = "subscription_android_sources";
      $table_name = "movies_sources_subscription_android";
      //log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['tv_channel']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
          //log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
      //  log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //tv_everywhere_ios_sources
      $resource_name = "tv_everywhere_ios_sources";
      $table_name = "movies_sources_tv_everywhere_ios";
      //log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['tv_channel']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              // log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
        //  log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
      //  log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }

      //tv_everywhere_android_sources
      $resource_name = "tv_everywhere_android_sources";
      $table_name = "movies_sources_tv_everywhere_android";
    //  log_message("info","[MOVIE CHANGED] Checking {$resource_name}");
      if(!empty($req[$resource_name]) && $req[$resource_name]!='[]'){
        $original_sources = $this->db->get_where($table_name,array("movie_id"=>$movie_id))->result_array();
        $new_sources = $req[$resource_name];
        //format new source response for easier comparison
        foreach($new_sources as $index=>$source){
          $new_sources[$index]['formats']="null";
          $new_sources[$index]['tv_channel']="null";
          $new_sources[$index]['movie_id']=$movie_id;
        }
        if(count($new_sources)!=count($original_sources)){
          log_message("info","[MOVIE CHANGED] {$resource_name} changed");
          foreach($new_sources as $new_source){
            $already_is=false;
            for($i=0; $i<count($original_sources);$i++){
              if($new_source['link']==$original_sources[$i]['link']){
                //source already saved, skipping
                $already_is = true;
                break;
              } else {
                $already_is = false;

              }
            }
            if($already_is){
              //log_message("info","[MOVIE CHANGED] {$new_source['source']} already in db");
            } else{
              $this->db->insert($table_name,array("movie_id"=>$movie_id,"source"=>$new_source['source'],"display_name"=>$new_source['display_name'],"tv_channel"=>"NULL", "app_name"=>$new_source['app_name'],"app_link"=>$new_source['app_link'],"app_required"=>$new_source['app_required'],"app_download_link"=>$new_source['app_download_link'],"link"=>$new_source['link'],"formats"=>"null"));
              log_message("info","[MOVIE CHANGED] New source added: {$new_source['source']}");
            }
          }
        } else {
        //  log_message("info","[MOVIE CHANGED] No updates for {$resource_name}");
        }
      }else{
        //log_message("info","[MOVIE CHANGED] {$resource_name} empty");
      }
      $this->updateAllSources($movie_id,"movie");
    }

    public function updateGuideboxRating(){
      //1. movies
      $rtgOffset = 0;
      while($rtgOffset<=760){
        $rtgUrl = $this->url."movies/all/".$rtgOffset."/250/all/all";
        $rtgParsed = json_decode(file_get_contents($rtgUrl),true);

        foreach($rtgParsed['results'] as $key=>$movie){
          $movie_id = $movie['id'];
          $movie_title = $movie['title'];
          $movie_rating = $key+$rtgOffset+1;
          $this->db->update("movies",array('guidebox_rating'=>$movie_rating),"id=".$movie_id);
          log_message("info","({$movie_id}) {$movie_title} on position {$movie_rating}");
        }
        //wait 1 second
        usleep(2000000);
        $rtgOffset+=250;
      }

      usleep(1000000);
      //2. shows
      $rtgOffset = 0;
      $rtgUrl = $this->url."shows/all/".$rtgOffset."/110/all/all";
      $rtgParsed = json_decode(file_get_contents($rtgUrl),true);

      foreach($rtgParsed['results'] as $key=>$movie){
        $movie_id = $movie['id'];
        $movie_title = $movie['title'];
        $movie_rating = $key+$rtgOffset+1;
        $this->db->update("shows",array('guidebox_rating'=>$movie_rating),"id=".$movie_id);
        //log_message("info","({$movie_id}) {$movie_title} on position {$movie_rating}");
      }
      //wait 1 second
    }
    /**
     * Make a request with cURL
     * * decode the incoming json
     * @param $url
     * @return array
     * @throws Exception
     */
    private function _curl($url)
    {

        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $curl = curl_init();
            curl_setopt_array($curl, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $url,
                    CURLOPT_USERAGENT => $this->user_agent
                ]
            );

            $json = curl_exec($curl);
            $this->http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $seconds = 5;
            while ($this->http_code == '500' || $this->http_code == '404') {
                log_message('error', "_curl($url) - RECEIVED HTTP STATUS CODE {$this->http_code}. Delaying for {$seconds} seconds. Message: " . $json);
                sleep($seconds);
                $seconds += 5;

                if ($seconds >= 120) {
                    log_message('error', "_curl($url) - RECEIVED HTTP STATUS CODE {$this->http_code}. Giving up after MORE THAN 120 seconds. Message: " . $json);
                    die();
                }
            }

            curl_close($curl);
            $f = json_decode($json, true);

            // Hide the number of remaining api calls
            // Can be used ONLY ON TEMP API KEYS
            //array_pop($f);

            // Returns an array
            return $f;
        }

        log_message('error', "_curl({$url}) - Invalid");
        throw new ErrorException('Empty or incorrect URL! >>> ' . $url);
    }


    /**
     *
     */
    public function update()
    {

        $url = $this->url . "";

        // get the DB timestamp
        $last_check = $this->db->query("SELECT last_check FROM updates_all order by last_check desc limit 1")->result_array();

        //if ($last_check) {}


        echo $this->debug($last_check);
    }


    /**
     * @throws ErrorException
     */
    public function persons()
    {

        $persons_collection = $this->db->query("
        SELECT person_id
        FROM movies_to_cast
        UNION ALL
        SELECT person_id
        FROM movies_to_directors
        UNION ALL
        SELECT person_id
        FROM movies_to_writers
        UNION ALL
        SELECT person_id
        FROM shows_to_cast
        UNION ALL
        SELECT person_id
        FROM episodes_to_cast
        UNION ALL
        SELECT person_id
        FROM episodes_to_directors
        UNION ALL
        SELECT person_id
        FROM episodes_to_guest_stars
        UNION
        SELECT person_id
        FROM episodes_to_writers
        GROUP BY person_id
        ")->result_array();


        foreach ($persons_collection as $arr => $item) {
            $db_check = (int)$this->db->get_where('persons', ['id' => $item['person_id']])->num_rows();

            // Not found in DB
            if ($db_check === 0) {
                $req = $this->_curl($this->url . "person/{$item['person_id']}");

                // Sometimes Guidebox doesn't have info on a person
                $req = array_filter($req);
                if (!empty($req['id'])) {

                    if (!empty($req['images']['medium'])) {
                        $req['images'] = $req['images']['medium']['url'];
                        $this->downloadImages($req['images']);
                    }

                    $this->Db->insert('persons', $req);
                }
                usleep(900000);
            }
        }


    }


    /**
     * Stores channels
     * @throws Exception
     */
    public function channels()
    {
        $limit1 = 0;
        $limit2 = 50;
        $url = $this->url . "channels/all/{$limit1}/{$limit2}";

        // Make a first request
        $req = $this->_curl($url);

        // Establish some limits based on first request
        $total_results = $req['total_results'];
        $total_results_returned = $req['total_returned'];
        $total_requests = ceil($total_results / $limit2);

        // Count the total requests
        for ($i = $total_requests; $i >= 0; $i--) {

            $url = $this->url . "channels/all/{$limit1}/{$limit2}";
            $data = $this->_curl($url);

            // Safety net: if we receive an empty response, break
            if (empty($data['results'])) {
                log_message('error', "/channels/all/{$limit1}/{$limit2} - RECEIVED EMPTY RESULTS. Limit1: {$limit1}. Requests {$i} of {$total_requests}.");
                break;
            }

            // Insert each item in DB
            for ($j = count($data['results']) - 1; $j >= 0; $j--) {

                if (!empty($this->Db->select('channels', $data['results'][$j]['id']))) {
                    $this->Db->update('channels', $data['results'][$j], $data['results'][$j]['id']);
                } else {
                    $this->Db->insert('channels', $data['results'][$j]);
                }
            }

            $limit1 += 50;
            usleep(500000);
        }

        echo "Finished with Limit1 = {$limit1}.";
        exit(0);
    }


    /**
     * Stores sources as a single DB table or separate
     * @param string $all
     * @throws Exception
     */
    public function sources($all = '')
    {

        // Process each source as a separate table
        if (empty($all)) {
            $sources = ['free', 'subscription', 'purchase', 'tv_everywhere'];

            foreach ($sources as $source) {
                $url = $this->url . "sources/{$source}/all";
                $data = $this->_curl($url);

                // Safety net: if we receive an empty response, break
                if (empty($data['results'])) {
                    log_message('error', "/sources_{$source}/all - RECEIVED EMPTY RESULTS RECEIVED.");
                    break;
                }

                for ($i = count($data['results']) - 1; $i >= 0; $i--) {
                    $this->Db->insert("sources_{$source}", $data['results'][$i]);
                }

                //file_put_contents("sources_{$source}.json", json_encode($data['results']));
                usleep(500000);
            }

        } else if ($all === 'all') {

            // Mash up all sources together into a single table, `sources`
            $url = $this->url . "sources/all/all";
            $data = $this->_curl($url);

            // Safety net: if we receive an empty response, break
            if (empty($data['results'])) {
                log_message('error', "/sources/all/all - RECEIVED EMPTY RESULTS RECEIVED.");
                return;
            }

            for ($i = count($data['results']) - 1; $i >= 0; $i--) {
                $this->Db->insert("sources", $data['results'][$i]);
            }

            //file_put_contents("sources.json", json_encode($data['results']));
        }

    }


    /**
     * @return int|false Number of bytes written | false on failure
     * @throws Exception
     */
    public function genres()
    {

        $url = $this->url . 'genres';
        $raw_data = $this->_curl($url)['results'];

        for ($i = count($raw_data) - 1; $i >= 0; $i--) {
            $q = $this->Db->insert('genres', $raw_data[$i]);
        }

        //return file_put_contents('genres.json', json_encode($raw_data));
        return $q;

    }


    /**
     * Store all tags
     * @throws Exception
     */
    public function tags()
    {

        $limit1 = 0;
        $limit2 = 250;
        $url = $this->url . "tags/{$limit1}/{$limit2}";

        // Make a first request
        $req = $this->_curl($url);

        // Establish some limits based on first request
        $total_results = $req['total_results'];
        $total_results_returned = $req['total_returned'];
        $total_requests = ceil($total_results / $limit2);

        // Count the total requests
        for ($i = $total_requests; $i >= 0; $i--) {

            $url = $this->url . "tags/{$limit1}/{$limit2}";
            $new_req = $this->_curl($url);

            // Safety net: if we receive an empty response, break
            if (empty($new_req['results'])) {
                log_message('error', "/store/tags - RECEIVED EMPTY RESULTS. Limit1: {$limit1}. Requests {$i} of {$total_requests}.");
                break;
            }

            // Insert each item in DB
            for ($j = count($new_req['results']) - 1; $j >= 0; $j--) {
                $this->Db->insert('tags', $new_req['results'][$j]);
            }

            $limit1 += 250;
            usleep(500000);
        }

        echo "Limit1: {$limit1}.";

    }


    /**
     * @param $id
     * @throws Exception
     */
    public function processMovieById($id)
    {
        $url = $this->url . 'movie/' . $id;
		//wait 1 second
        usleep(1000000);

        // No need to use ['results']
        $req = $this->_curl($url);

        // Safety net
        if (empty($req)) {
            log_message('error', "/movies/id - RECEIVED EMPTY RESULTS. Stopped at movie id {$id}.");
            die();
        }


        /*
         * TODO: make it a method
         */

        // movies -> ... movies
        $data = [];
        $table_columns = $this->db->query('DESCRIBE movies')->result_array();
        foreach ($table_columns as $index => $val) {
			if (array_key_exists($val['Field'],$req)){
				$data[$val['Field']] = $req[$val['Field']];
			}
        }
        $this->Db->insert('movies', $data);

        // genres -> movies_to_genres
        if (!empty($req['genres']) && $req['genres'] != '[]') {
            for ($i = count($req['genres']) - 1; $i >= 0; $i--) {
                $this->db->query("
              insert into movies_to_genres
              (movie_id, genre_id)
              values
              (
              {$this->db->escape($req['id'])},
              {$this->db->escape($req['genres'][$i]['id'])}
              )
            ");
            }
        }

        // tags -> movies_to_tags
        if (!empty($req['tags']) && $req['tags'] != '[]') {
            for ($i = count($req['tags']) - 1; $i >= 0; $i--) {
                $this->db->query("
              insert into movies_to_tags
              (movie_id, tag_id)
              values
              (
              {$this->db->escape($req['id'])},
              {$this->db->escape($req['tags'][$i]['id'])}
              )
            ");
            }
        }

        // writers -> movies_to_writers
        if (!empty($req['writers']) && $req['writers'] != '[]') {
            for ($i = count($req['writers']) - 1; $i >= 0; $i--) {
                $this->db->query("
              insert into movies_to_writers
              (`movie_id`, `person_id`)
              values
              (
              {$this->db->escape($req['id'])},
              {$this->db->escape($req['writers'][$i]['id'])}
              )
            ");
            }
        }

        // directors -> movies_to_directors
        if (!empty($req['directors']) && $req['genres'] != '[]') {
            for ($i = count($req['directors']) - 1; $i >= 0; $i--) {
                $this->db->query("
              insert into movies_to_directors
              (`movie_id`, `person_id`)
              values
              (
              {$this->db->escape($req['id'])},
              {$this->db->escape($req['directors'][$i]['id'])}
              )
            ");
            }
        }

        // cast -> movies_to_cast
        if (!empty($req['cast']) && $req['cast'] != '[]') {
            for ($i = count($req['cast']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_to_cast
                (movie_id, person_id, character_name)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['cast'][$i]['id'])},
                {$this->db->escape($req['cast'][$i]['character_name'])}
                )
            ");
            }
        }

        // free_web_sources -> movies_sources_free_web
        if (!empty($req['free_web_sources']) && $req['free_web_sources'] != '[]') {
            for ($i = count($req['free_web_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_free_web
                (movie_id, source, display_name, link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['free_web_sources'][$i]['source'])},
                {$this->db->escape($req['free_web_sources'][$i]['display_name'])},
                {$this->db->escape($req['free_web_sources'][$i]['link'])},
                " . $this->db->escape(json_encode($req['free_web_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // tv_everywhere_web_sources -> movies_sources_tv_everywhere_web
        if (!empty($req['tv_everywhere_web_sources']) && $req['tv_everywhere_web_sources'] != '[]') {
            for ($i = count($req['tv_everywhere_web_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_tv_everywhere_web
                (movie_id, source, display_name, link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['tv_everywhere_web_sources'][$i]['source'])},
                {$this->db->escape($req['tv_everywhere_web_sources'][$i]['display_name'])},
                {$this->db->escape($req['tv_everywhere_web_sources'][$i]['link'])},
                " . $this->db->escape(json_encode($req['tv_everywhere_web_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // subscription_web_sources -> movies_sources_subscription_web
        if (!empty($req['subscription_web_sources']) && $req['subscription_web_sources'] != '[]') {
            for ($i = count($req['subscription_web_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_subscription_web
                (movie_id, source, display_name, link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['subscription_web_sources'][$i]['source'])},
                {$this->db->escape($req['subscription_web_sources'][$i]['display_name'])},
                {$this->db->escape($req['subscription_web_sources'][$i]['link'])},
                " . $this->db->escape(json_encode($req['subscription_web_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // purchase_web_sources -> movies_sources_purchase_web
        if (!empty($req['purchase_web_sources']) && $req['purchase_web_sources'] != '[]') {
            for ($i = count($req['purchase_web_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_purchase_web
                (movie_id, source, display_name, link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['purchase_web_sources'][$i]['source'])},
                {$this->db->escape($req['purchase_web_sources'][$i]['display_name'])},
                {$this->db->escape($req['purchase_web_sources'][$i]['link'])},
                " . $this->db->escape(json_encode($req['purchase_web_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // other_sources -> movies_sources_other
        if (!empty($req['other_sources']['tv_on_demand']) && $req['other_sources']['tv_on_demand'] != '[]') {
            for ($i = count($req['other_sources']['tv_on_demand']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_other
                (movie_id, source, source_type, display_name, link, platform, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['other_sources']['tv_on_demand'][$i]['source'])},
                {$this->db->escape($req['other_sources']['tv_on_demand'][$i]['source_type'])},
                {$this->db->escape($req['other_sources']['tv_on_demand'][$i]['display_name'])},
                {$this->db->escape($req['other_sources']['tv_on_demand'][$i]['link'])},
                {$this->db->escape($req['other_sources']['tv_on_demand'][$i]['platform'])},
                " . $this->db->escape(json_encode($req['other_sources']['tv_on_demand'][$i]['formats'])) . "
                )
            ");
            }
        }

        // purchase_ios_sources -> movies_sources_purchase_ios
        if (!empty($req['purchase_ios_sources']) && $req['purchase_ios_sources'] != '[]') {
            for ($i = count($req['purchase_ios_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_purchase_ios
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['purchase_ios_sources'][$i]['source'])},
                {$this->db->escape($req['purchase_ios_sources'][$i]['display_name'])},
                {$this->db->escape($req['purchase_ios_sources'][$i]['link'])},
                {$this->db->escape($req['purchase_ios_sources'][$i]['app_name'])},
                {$this->db->escape($req['purchase_ios_sources'][$i]['app_link'])},
                {$this->db->escape($req['purchase_ios_sources'][$i]['app_required'])},
                {$this->db->escape($req['purchase_ios_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['purchase_ios_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // purchase_android_sources -> movies_sources_purchase_android
        if (!empty($req['purchase_android_sources']) && $req['purchase_android_sources'] != '[]') {
            for ($i = count($req['purchase_android_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_purchase_android
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['purchase_android_sources'][$i]['source'])},
                {$this->db->escape($req['purchase_android_sources'][$i]['display_name'])},
                {$this->db->escape($req['purchase_android_sources'][$i]['link'])},
                {$this->db->escape($req['purchase_android_sources'][$i]['app_name'])},
                {$this->db->escape($req['purchase_android_sources'][$i]['app_link'])},
                {$this->db->escape($req['purchase_android_sources'][$i]['app_required'])},
                {$this->db->escape($req['purchase_android_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['purchase_android_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        //
        // free_android_sources -> movies_sources_free_android
        if (!empty($req['free_android_sources']) && $req['free_android_sources'] != '[]') {
            for ($i = count($req['free_android_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_free_android
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['free_android_sources'][$i]['source'])},
                {$this->db->escape($req['free_android_sources'][$i]['display_name'])},
                {$this->db->escape($req['free_android_sources'][$i]['link'])},
                {$this->db->escape($req['free_android_sources'][$i]['app_name'])},
                {$this->db->escape($req['free_android_sources'][$i]['app_link'])},
                {$this->db->escape($req['free_android_sources'][$i]['app_required'])},
                {$this->db->escape($req['free_android_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['free_android_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // free_ios_sources -> movies_sources_free_ios
        if (!empty($req['free_ios_sources']) && $req['free_ios_sources'] != '[]') {
            for ($i = count($req['free_ios_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_free_ios
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['free_ios_sources'][$i]['source'])},
                {$this->db->escape($req['free_ios_sources'][$i]['display_name'])},
                {$this->db->escape($req['free_ios_sources'][$i]['link'])},
                {$this->db->escape($req['free_ios_sources'][$i]['app_name'])},
                {$this->db->escape($req['free_ios_sources'][$i]['app_link'])},
                {$this->db->escape($req['free_ios_sources'][$i]['app_required'])},
                {$this->db->escape($req['free_ios_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['free_ios_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // subscription_ios_sources -> movies_sources_subscription_ios
        if (!empty($req['subscription_ios_sources']) && $req['subscription_ios_sources'] != '[]') {
            for ($i = count($req['subscription_ios_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_subscription_ios
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['subscription_ios_sources'][$i]['source'])},
                {$this->db->escape($req['subscription_ios_sources'][$i]['display_name'])},
                {$this->db->escape($req['subscription_ios_sources'][$i]['link'])},
                {$this->db->escape($req['subscription_ios_sources'][$i]['app_name'])},
                {$this->db->escape($req['subscription_ios_sources'][$i]['app_link'])},
                {$this->db->escape($req['subscription_ios_sources'][$i]['app_required'])},
                {$this->db->escape($req['subscription_ios_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['subscription_ios_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // subscription_android_sources -> movies_sources_subscription_android
        if (!empty($req['subscription_android_sources']) && $req['subscription_android_sources'] != '[]') {
            for ($i = count($req['subscription_android_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_subscription_android
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['subscription_android_sources'][$i]['source'])},
                {$this->db->escape($req['subscription_android_sources'][$i]['display_name'])},
                {$this->db->escape($req['subscription_android_sources'][$i]['link'])},
                {$this->db->escape($req['subscription_android_sources'][$i]['app_name'])},
                {$this->db->escape($req['subscription_android_sources'][$i]['app_link'])},
                {$this->db->escape($req['subscription_android_sources'][$i]['app_required'])},
                {$this->db->escape($req['subscription_android_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['subscription_android_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // tv_everywhere_ios_sources -> movies_sources_tv_everywhere_ios
        if (!empty($req['tv_everywhere_ios_sources']) && $req['tv_everywhere_ios_sources'] != '[]') {
            for ($i = count($req['tv_everywhere_ios_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_tv_everywhere_ios
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['tv_everywhere_ios_sources'][$i]['source'])},
                {$this->db->escape($req['tv_everywhere_ios_sources'][$i]['display_name'])},
                {$this->db->escape($req['tv_everywhere_ios_sources'][$i]['link'])},
                {$this->db->escape($req['tv_everywhere_ios_sources'][$i]['app_name'])},
                {$this->db->escape($req['tv_everywhere_ios_sources'][$i]['app_link'])},
                {$this->db->escape($req['tv_everywhere_ios_sources'][$i]['app_required'])},
                {$this->db->escape($req['tv_everywhere_ios_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['tv_everywhere_ios_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // tv_everywhere_android_sources -> movies_sources_tv_everywhere_android
        if (!empty($req['tv_everywhere_android_sources']) && $req['tv_everywhere_android_sources'] != '[]') {
            for ($i = count($req['tv_everywhere_android_sources']) - 1; $i >= 0; $i--) {
                $this->db->query("
                insert into movies_sources_tv_everywhere_android
                (movie_id, source, display_name, link, app_name, app_link, app_required, app_download_link, formats)
                values
                (
                {$this->db->escape($req['id'])},
                {$this->db->escape($req['tv_everywhere_android_sources'][$i]['source'])},
                {$this->db->escape($req['tv_everywhere_android_sources'][$i]['display_name'])},
                {$this->db->escape($req['tv_everywhere_android_sources'][$i]['link'])},
                {$this->db->escape($req['tv_everywhere_android_sources'][$i]['app_name'])},
                {$this->db->escape($req['tv_everywhere_android_sources'][$i]['app_link'])},
                {$this->db->escape($req['tv_everywhere_android_sources'][$i]['app_required'])},
                {$this->db->escape($req['tv_everywhere_android_sources'][$i]['app_download_link'])},
                " . $this->db->escape(json_encode($req['tv_everywhere_android_sources'][$i]['formats'])) . "
                )
            ");
            }
        }

        // Process any images that we might find.
        //$_img_ex = $req;
        //$this->downloadImages($_img_ex);
        $this->updateAllSources($id,"movie");
    }


    /**
     * @param $movie_id
     * @throws Exception
     */
    public function processMovieTrailers($movie_id)
    {
        $url = $this->url . "movie/{$movie_id}/videos/all/0/50";

        usleep(1000000);
        $data = $this->_curl($url)['results'];

        for ($i = count($data) - 1; $i >= 0; $i--) {
            $this->Db->insert('movies_trailers', $data[$i], 'movie_id', $movie_id);
        }

        // Process any images that we might find.
        $_img_ex = $data;
        $this->downloadImages($_img_ex);
    }


    /**
     * @param $movie_id
     * @throws Exception
     */
    public function processMovieImages($movie_id)
    {
        $url = $this->url . "movie/{$movie_id}/images/all";

        usleep(1000000);
        $data = $this->_curl($url)['results'];
        $this->Db->insert('movies_images', $data, 'movie_id', $movie_id);

        // Process any images that we might find.
        $_img_ex = $data;
        $this->downloadImages($_img_ex);
    }


    /**
     * @param $id
     */
    public function deleteMovie($id)
    {

        // TODO: on delete -> cascade

        $movies_db = [
            'movies_images',
            'movies_sources_free_android',
            'movies_sources_free_ios',
            'movies_sources_free_web',
            'movies_sources_other',
            'movies_sources_purchase_android',
            'movies_sources_purchase_ios',
            'movies_sources_purchase_web',
            'movies_sources_subscription_android',
            'movies_sources_subscription_ios',
            'movies_sources_subscription_web',
            'movies_sources_tv_everywhere_android',
            'movies_sources_tv_everywhere_ios',
            'movies_to_cast',
            'movies_to_directors',
            'movies_to_genres',
            'movies_to_tags',
            'movies_trailers',
        ];


        foreach ($movies_db as $db) {
            $this->db->delete($db, ['movie_id' => $id]);
        }

        return $this->db->delete('movies', ['id' => $id]);

    }


    /**
     * @param $id
     */
    public function deleteShow($id)
    {

        // TODO: on delete -> cascade

        $shows_db = [
            'shows_to_cast',
            'shows_to_channels',
            'shows_to_genres',
            'shows_to_images',
            'shows_to_related',
            'shows_to_seasons',
            'shows_to_tags',
        ];

        foreach ($shows_db as $db) {
            $this->db->delete($db, ['show_id' => $id]);
        }

        return $this->db->delete("shows", ['id' => $id]);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function deleteEpisode($id)
    {

        // TODO: on delete -> cascade

        $episodes_db = [
            'episodes_sources_free_android',
            'episodes_sources_free_ios',
            'episodes_sources_free_web',
            'episodes_sources_purchase_android',
            'episodes_sources_purchase_ios',
            'episodes_sources_purchase_web',
            'episodes_sources_subscription_android',
            'episodes_sources_subscription_ios',
            'episodes_sources_subscription_web',
            'episodes_sources_tv_everywhere_android',
            'episodes_sources_tv_everywhere_ios',
            'episodes_sources_tv_everywhere_web',
            'episodes_to_cast',
            'episodes_to_directors',
            'episodes_to_guest_stars',
            'episodes_to_writers',
        ];

        foreach ($episodes_db as $db) {
            $this->db->delete($db, ['episode_id' => $id]);
        }

        return $this->db->delete("episodes", ['id' => $id]);
    }


    /**
     * @throws Exception
     */
    public function movies()
    {

        $limit1 = 0;
        $limit2 = 250;

        // Gather all movies ids
        $movies_ids = [];

        // We'll only get the first 1000 movies (250 movies per request * 4 requests)
        for ($i = 3; $i >= 0; $i--) {

            $req = $this->_curl($this->url . "movies/all/{$limit1}/{$limit2}/all/all");

            if (empty($req['results']) || empty($req)) {
                log_message('error', "/movies/all - RECEIVED EMPTY RESULTS. Limit1: {$limit1}/1000. Requests {$i}/4.");
                break;
            }

            // Make a list of movies ids from the received response
            // Also check in DB if exists and narrow the list down
            //for ($j = count($req['results']) - 1; $j >= 0; $j--) {
            for ($j = 0; $j <count($req['results']); $j++) {
                if (empty($this->Db->select('movies', $req['results'][$j]['id']))) {
                    $movies_ids[] = $req['results'][$j]['id'];
                }
            }

            // If we have any movies, process them
            if (!empty($movies_ids)) {
                // Extended info about each movie will be handled separately
                foreach ($movies_ids as $id) {
                    $this->processMovieById($id);
                    $this->processMovieTrailers($id);
                    $this->processMovieImages($id);
                }

                // Process any images that we might find.
                $_img_ex = $req;
                $this->downloadImages($_img_ex);
            }

            $limit1 += 250;
            usleep(500000);
        }

        echo "Limit1 hit at: {$limit1}.";
    }


    /**
     * @throws ErrorException
     */
    public function shows()
    {
        $limit1 = 0;
        $limit2 = 100;

        $new_data = $this->_curl($this->url . "shows/all/{$limit1}/{$limit2}/all/all");

        if (empty($new_data['results']) || empty($new_data)) {
            log_message('error', "shows/all/{$limit1}/{$limit2}/all/all - RECEIVED EMPTY RESULTS. Limit1: {$limit1}.");
            die();
        }

        // Make a list of movies ids from the received response
        //
        $shows_ids = [];
        //for ($j = count($new_data['results']) - 1; $j >= 0; $j--) {
        for ($j = 0; $j <count($new_data['results']); $j++) {
            if (empty($this->Db->select('shows', $new_data['results'][$j]['id']))) {
                $shows_ids[] = $new_data['results'][$j]['id'];
            }
        }


        // Extended info about each show will be handled separately
        foreach ($shows_ids as $id) {
            $this->processShowById($id);

        }

        // Process any images that we might find.
        $_img_ex = $new_data;
        $this->downloadImages($_img_ex);

        echo "Limit1: {$limit1}.";
    }


    /**
     * @param $show_id
     * @throws ErrorException
     */
    public function processShowById($show_id)
    {

        $url = $this->url . 'shows/' . $show_id;
        usleep(500000);

        // No need to use ['results']
        $req = $this->_curl($url);

        // Safety net
        if (empty($req)) {
            log_message('error', "/shows/id - RECEIVED EMPTY RESULTS. Stopped at show id {$show_id}.");
            die();
        }

        // shows
        $data = [];
        $table_columns = $this->db->query('DESCRIBE shows')->result_array();
        foreach ($table_columns as $index => $val) {
          if (array_key_exists($val['Field'],$req)){
            $data[$val['Field']] = $req[$val['Field']];
          }
        }
        $this->Db->insert('shows', $data);

        // shows_to_cast
        $data = [];
        for ($i = count($req['cast']) - 1; $i >= 0; $i--) {
            $data['person_id'] = $req['cast'][$i]['id'];
            $data['character_name'] = $req['cast'][$i]['character_name'];
            $this->Db->insert('shows_to_cast', $data, 'show_id', $show_id);
        }

        // shows_to_tags
        $data = [];
        for ($i = count($req['tags']) - 1; $i >= 0; $i--) {
            $data['tag_id'] = $req['tags'][$i]['id'];
            $this->Db->insert('shows_to_tags', $data, 'show_id', $show_id);
        }

        // shows_to_genres
        $data = [];
        for ($i = count($req['genres']) - 1; $i >= 0; $i--) {
            $data['genre_id'] = $req['genres'][$i]['id'];
            $this->Db->insert('shows_to_genres', $data, 'show_id', $show_id);
        }

        // shows_to_channels
        $data = [];
        for ($i = count($req['channels']) - 1; $i >= 0; $i--) {
            $data['channel_id'] = $req['channels'][$i]['id'];
            $data['name'] = $req['channels'][$i]['name'];
            $data['short_name'] = $req['channels'][$i]['short_name'];
            $data['channel_type'] = $req['channels'][$i]['channel_type'];
            $data['is_primary'] = $req['channels'][$i]['is_primary'];
            $this->Db->insert('shows_to_channels', $data, 'show_id', $show_id);
        }


        /*
         * After a single show has been processed,
         * we break it down on seasons/episodes of that show
         */
        $this->processShowToSeasons($show_id);
        $this->processShowToRelated($show_id);
        $this->processShowImages($show_id);
        $this->processShowEpisodes($show_id);

        // Process any images that we might find.
        $_img_ex = $req;
        $this->downloadImages($_img_ex);
        $this->updateAllSources($show_id,"show");
    }

    // 1. shows_to_seasons
    /**
     * @param $show_id
     * @throws ErrorException
     */
    public function processShowToSeasons($show_id)
    {
        $url = $this->url . "show/{$show_id}/seasons";
        usleep(500000);

        $req = $this->_curl($url)['results'];

        $data = [];
        for ($i = count($req) - 1; $i >= 0; $i--) {
            $data['season_number'] = $req[$i]['season_number'];
            $data['first_airdate'] = $req[$i]['first_airdate'];
            $this->Db->insert('shows_to_seasons', $data, 'show_id', $show_id);
        }

    }

    // 2. shows_to_related - show/{$show_id}/related
    /**
     * @param $show_id
     * @throws ErrorException
     */
    public function processShowToRelated($show_id)
    {
        $url = $this->url . "show/{$show_id}/related";
        usleep(500000);

        $req = $this->_curl($url)['results'];

        $data = [];
        for ($i = count($req) - 1; $i >= 0; $i--) {
            $data['related_show_id'] = $req[$i]['id'];
            $this->Db->insert('shows_to_related', $data, 'show_id', $show_id);
        }
    }

    // 3. shows_to_images - show/{$show_id}/images/all
    /**
     * @param $show_id
     * @throws ErrorException
     */
    public function processShowImages($show_id)
    {
        $url = $this->url . "show/{$show_id}/images/all";
        usleep(500000);

        $req = $this->_curl($url)['results'];

        $data = [];
        $data['thumbnails'] = $req['thumbnails'];
        $data['posters'] = $req['posters'];
        $data['banners'] = $req['banners'];
        $data['backgrounds'] = $req['backgrounds'];
        echo $this->Db->insert('shows_to_images', $data, 'show_id', $show_id);

        // Process any images that we might find.
        $_img_ex = $req;
        $this->downloadImages($_img_ex);

    }

    // 4. shows_to_episodes - show/{$show_id}/episodes/all/0/100/all/all
    /**
     * @param $show_id
     * @throws ErrorException
     */
    public function processShowEpisodes($show_id)
    {
        $url = $this->url . "show/{$show_id}/episodes/all/0/100/all/all";
        usleep(500000);

        $req = $this->_curl($url);

        // Get a list of episodes (ids)
        $episodes_ids = [];
        for ($j = count($req['results']) - 1; $j >= 0; $j--) {
            if (empty($this->Db->select('episodes', $req['results'][$j]['id']))) {
                $episodes_ids[] = $req['results'][$j]['id'];
            }
        }

        // Process each episode
        foreach ($episodes_ids as $episode_id) {
            $this->processEpisode($episode_id);
        }

        // Process any images that we might find.
        $_img_ex = $req;
        $this->downloadImages($_img_ex);

    }

    /**
     * @param $episode_id
     * @throws ErrorException
     */
    public function processEpisode($episode_id)
    {
        $url = $this->url . "episode/{$episode_id}";
        usleep(500000);

        // No need for ['results']
        $req = $this->_curl($url);

        // Store the episode
        $data = [];
        $table_columns = $this->db->query('DESCRIBE episodes')->result_array();
        foreach ($table_columns as $index => $val) {
          if (array_key_exists($val['Field'],$req)){
            $data[$val['Field']] = $req[$val['Field']];
          }
        }
        $this->Db->insert('episodes', $data);

        /*
         * Break down the episode into pieces
         */

        // episodes_to_cast
        if (!empty($req['cast']) && $req['cast'] != '[]') {
            $data = [];
            for ($i = count($req['cast']) - 1; $i >= 0; $i--) {
                $data['person_id'] = $req['cast'][$i]['id'];
                $data['character_name'] = $req['cast'][$i]['character_name'];
                $this->Db->insert('episodes_to_cast', $data, 'episode_id', $episode_id);
            }
        }

        // episodes_to_writers
        if (!empty($req['writers']) && $req['writers'] != '[]') {
            $data = [];
            for ($i = count($req['writers']) - 1; $i >= 0; $i--) {
                $data['person_id'] = $req['writers'][$i]['id'];
                $this->Db->insert('episodes_to_writers', $data, 'episode_id', $episode_id);
            }
        }

        // episodes_to_directors
        $data = [];
        for ($i = count($req['directors']) - 1; $i >= 0; $i--) {
            $data['person_id'] = $req['directors'][$i]['id'];
            $this->Db->insert('episodes_to_directors', $data, 'episode_id', $episode_id);
        }

//        echo $this->debug($req);
//        die();
        // episodes_to_guest_stars
        if (!empty($req['guest_stars']) && $req['guest_stars'] != '[]') {
            $data = [];
            for ($i = count($req['guest_stars']) - 1; $i >= 0; $i--) {
                $data['person_id'] = $req['guest_stars'][$i]['id'];
                $data['character_name'] = $req['guest_stars'][$i]['character_name'];
                $this->Db->insert('episodes_to_guest_stars', $data, 'episode_id', $episode_id);
            }
        }

        /*
         * FREE
         */
         $getShowId = $this->episodeIDtoShowID($episode_id);
        // free_ios_sources -> episodes_sources_free_ios
        if (!empty($req['free_ios_sources']) && $req['free_ios_sources'] != '[]') {
            $data = [];
            $_f = $req['free_ios_sources'];
            for ($i = count($req['free_ios_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['formats'] = "NULL";
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_free_ios', $data, 'episode_id', $episode_id);
            }
        }

        // free_android_sources -> episodes_sources_free_android
        if (!empty($req['free_android_sources']) && $req['free_android_sources'] != '[]') {
            $data = [];
            $_f = $req['free_android_sources'];
            for ($i = count($req['free_android_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['formats'] = "NULL";
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_free_android', $data, 'episode_id', $episode_id);
            }
        }

        // free_web_sources -> episodes_sources_free_web
        if (!empty($req['free_web_sources']) && $req['free_web_sources'] != '[]') {
            $data = [];
            $_f = $req['free_web_sources'];
            for ($i = count($req['free_web_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_free_web', $data, 'episode_id', $episode_id);
            }
        }

        /*
         * Web or desktop
         */

        // subscription_web_sources -> episodes_sources_subscription_web
        if (!empty($req['subscription_web_sources']) && $req['subscription_web_sources'] != '[]') {
            $data = [];
            $_f = $req['subscription_web_sources'];
            for ($i = count($req['subscription_web_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_subscription_web', $data, 'episode_id', $episode_id);
            }
        }

        // purchase_web_sources -> episodes_sources_purchase_web
        if (!empty($req['purchase_web_sources']) && $req['purchase_web_sources'] != '[]') {
            $data = [];
            $_f = $req['purchase_web_sources'];
            for ($i = count($req['purchase_web_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['formats'] = $_f[$i]['formats'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_purchase_web', $data, 'episode_id', $episode_id);
            }
        }

        // tv_everywhere_web_sources -> episodes_sources_tv_everywhere_web
        if (!empty($req['tv_everywhere_web_sources']) && $req['tv_everywhere_web_sources'] != '[]') {
            $data = [];
            $_f = $req['tv_everywhere_web_sources'];
            for ($i = count($req['tv_everywhere_web_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_tv_everywhere_web', $data, 'episode_id', $episode_id);
            }
        }

        /*
         * Mobile
         */

        // subscription_ios_sources -> episodes_sources_subscription_ios
        if (!empty($req['subscription_ios_sources']) && $req['subscription_ios_sources'] != '[]') {
            $data = [];
            $_f = $req['subscription_ios_sources'];
            for ($i = count($req['subscription_ios_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_subscription_ios', $data, 'episode_id', $episode_id);
            }
        }

        // subscription_android_sources -> episodes_sources_subscription_android
        if (!empty($req['subscription_android_sources']) && $req['subscription_android_sources'] != '[]') {
            $data = [];
            $_f = $req['subscription_android_sources'];
            for ($i = count($req['subscription_android_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_subscription_android', $data, 'episode_id', $episode_id);
            }
        }

        // purchase_android_sources -> episodes_sources_purchase_android
        if (!empty($req['purchase_android_sources']) && $req['purchase_android_sources'] != '[]') {
            $data = [];
            $_f = $req['purchase_android_sources'];
            for ($i = count($req['purchase_android_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['formats'] = $_f[$i]['formats'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_purchase_android', $data, 'episode_id', $episode_id);
            }
        }

        // purchase_ios_sources -> episodes_sources_purchase_ios
        if (!empty($req['purchase_ios_sources']) && $req['purchase_ios_sources'] != '[]') {
            $data = [];
            $_f = $req['purchase_ios_sources'];
            for ($i = count($req['purchase_ios_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['formats'] = $_f[$i]['formats'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_purchase_ios', $data, 'episode_id', $episode_id);
            }
        }

        // tv_everywhere_ios_sources -> episodes_sources_tv_everywhere_ios
        if (!empty($req['tv_everywhere_ios_sources']) && $req['tv_everywhere_ios_sources'] != '[]') {
            $data = [];
            $_f = $req['tv_everywhere_ios_sources'];
            for ($i = count($req['tv_everywhere_ios_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_tv_everywhere_ios', $data, 'episode_id', $episode_id);
            }
        }

        // tv_everywhere_android_sources -> episodes_sources_tv_everywhere_android
        if (!empty($req['tv_everywhere_android_sources']) && $req['tv_everywhere_android_sources'] != '[]') {
            $data = [];
            $_f = $req['tv_everywhere_android_sources'];
            for ($i = count($req['tv_everywhere_android_sources']) - 1; $i >= 0; $i--) {
                $data['source'] = $_f[$i]['source'];
                $data['display_name'] = $_f[$i]['display_name'];
                $data['id'] = $_f[$i]['id'];
                $data['link'] = $_f[$i]['link'];
                $data['app_name'] = $_f[$i]['app_name'];
                $data['app_link'] = $_f[$i]['app_link'];
                $data['app_required'] = $_f[$i]['app_required'];
                $data['app_download_link'] = $_f[$i]['app_download_link'];
                $data['show_id']=$getShowId;
                $this->Db->insert('episodes_sources_tv_everywhere_android', $data, 'episode_id', $episode_id);
            }
        }


        // Process any images that we might find.
        $_img_ex = $req;
        $this->downloadImages($_img_ex);

    }

    public function updateAllMovieSources(){
      $res = $this->db->get('movies')->result_array();
      foreach($res as $movie){
        log_message("info","Updating sources for {$movie['title']}");
        $this->updateAllSources($movie['id'],"movie");
      }
    }
    public function updateAllShowsSources(){
      $res = $this->db->get('shows')->result_array();
      foreach($res as $show){
        log_message("info","Updating sources for {$show['title']}");
        $this->updateAllSources($show['id'],"show");
      }
    }

}
