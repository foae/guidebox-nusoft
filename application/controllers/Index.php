<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Trait.Helper.php';

class Index extends CI_Controller
{
    use Helper;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['memory'] = $this->human_filesize(memory_get_usage());
        $data['load_avg'] = implode(' / ', sys_getloadavg());
        $this->load->view('index', $data);
    }
}
