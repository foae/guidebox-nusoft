<?php

// Tweak the execution time
set_time_limit(36000);
ini_set('max_input_time', 36000);
ini_set('max_execution_time', 36000);

/**
 * Class Helper
 */
Trait Helper
{


    /**
     * Joins two existing json files on [] and returns it as one
     * @param $file1
     * @param $file2
     * @return string json
     */
    public function joinJsonFromFile($file1, $file2)
    {

        $file1 = file_get_contents($file1);
        $file2 = file_get_contents($file2);

        return json_encode(array_merge(json_decode($file1, true), json_decode($file2, true)));
    }


    /**
     * Join a local stored json with one from a stream
     * @param $file
     * @param $stream
     * @return string json
     * @throws ErrorException
     */
    public function joinJsonFromStream($file, $stream)
    {

        if (file_exists($file) === false) {
            throw new ErrorException("File doesn't exist: {$file}");
        }

        $local_file = file_get_contents($file);
        return json_encode(array_merge(json_decode($local_file, true), json_decode($stream, true)));
    }


    /**
     * Merge two json files from memory
     * @param $json1
     * @param $json2
     * @return string json
     */
    public function joinJson($json1, $json2)
    {
        return json_encode(array_merge(json_decode($json1, true), json_decode($json2, true)));
    }


    /**
     * Display file size in human readable format
     * @param $bytes
     * @param int $decimals
     * @return string
     */
    public function human_filesize($bytes, $decimals = 2)
    {
        $size = array(' B', ' kB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[(int)$factor];
    }


    /**
     * Format and display output
     * @param $data
     * @return string
     */
    public function debug($data)
    {
        return PHP_EOL . '<pre>' . print_r($data, true) . '</pre>' . PHP_EOL;
    }


    /**
     * Saves a file locally from a given URL
     * @param $url
     * @param $location
     * @param $id
     * @return bool
     */
    public function saveFileFromUrl($url, $location = '', $id)
    {

        // url - a classic
        // location - where to save the img (builds the $path)
        // movie_id - the folder in which the filename will be saved

        $path = FCPATH . '_img/';

        if ($location === 'movies') {
            $path .= 'movies/';
        } elseif ($location === 'shows') {
            $path .= 'shows/';
        }

        $highest_index = max(array_keys(explode('/', $url)));
        $filename = explode('/', $url)[$highest_index];
        return copy($url, $path . '/' . $filename);

    }


    /**
     * Scans strings/arrays (not objects) and strips the guidebox domain
     * Returns back the data in its original data type
     * @param $q
     * @return mixed|string
     */
    public function stripGuideboxDomain($q)
    {

        $q = json_encode($q);
        $q = str_replace('http://static-api.guidebox.com/', '', $q);
        $q = str_replace('http:\/\/static-api.guidebox.com\/', '', $q);
        $q = json_decode($q, true);

        return $q;

    }


    /**
     * Download image from URL to local storage
     * Creates path if it does not exists
     * Existing images will be overwritten
     * @param $data
     */
    public function downloadImages($data)
    {

        $pattern2 = '/http?:\/\/[^ ]+?(?:\.jpg|\.png|\.gif)/';
        $pattern = '/(http:\/\/[^"]*?\.(jpg|png))/';
        $collection = '';

        if (is_array($data)) {
            $data = serialize($data);
        }

        /*
        if (json_decode($data)) {
            $data = serialize(json_decode($data, true));
        }
        */

        preg_match_all($pattern, $data, $collection);

        if (!empty($collection[0])) {

            $collection = $collection[0];

            for ($i = count($collection) - 1; $i >= 0; $i--) {
                $this->createPath($this->stripGuideboxDomain($collection[$i]));
                copy($collection[$i], '_img/' . $this->stripGuideboxDomain($collection[$i]));
                usleep(150000);
            }
        }

    }


    /**
     * Create path on local storage from URL
     * @param $filename
     * @return bool|string
     */
    public function createPath($filename)
    {

        if (is_string($filename)) {

            $_f = explode('/', $filename);
            array_pop($_f);
            $file_path = FCPATH . '_img/' . implode('/', $_f);

            if (!file_exists($file_path)) {
                mkdir($file_path, 0777, true);
                chmod($file_path, 0777);
            }

            return $file_path;

        } else {
            log_message('error', 'createPath received a non string value for $filename: ' . print_r($filename, true));
            return false;
        }

    }

}