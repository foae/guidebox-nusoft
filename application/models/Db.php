<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH . 'application/controllers/Trait.Helper.php';


/**
 * Class Db
 */
class Db extends CI_Model
{

    use Helper;

    /**
     * Db constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param $table
     * @param $data
     * @param string $type
     * @param string $id
     * @return mixed
     * @throws ErrorException
     */
    public function insert($table, $data, $type = 'movie_id', $id = '')
    {


        $table_columns = serialize($this->db->query("DESCRIBE {$table}")->result_array());

        // Detect if a table has guidebox_id in it and add it
        if (strpos($table_columns, 'guidebox_id') !== false) {
            $data['guidebox_id'] = $data['id'];
        }

        // Detect if the current table needs a timestamp
        if (strpos($table_columns, 'timestamp') !== false) {
            $data['timestamp'] = date('Y-m-d H:i:s');
        }

        // Add a movie_id or show_id if needed
        if (!empty($id)) {
            if ($type === 'movie_id') {
                $data['movie_id'] = $id;
            } else if ($type === 'show_id') {
                $data['show_id'] = $id;
            } else if ($type === 'episode_id') {
                $data['episode_id'] = $id;
            } else {
                log_message('error', "Failed Model->Db->insert with type = {$type} and id = {$id}");
                throw new ErrorException("Failed Model->Db->insert with type = {$type} and id = {$id}");
            }
        }


        // Arrays will be stored as json
        if (is_array($data)) {

            // Store arrays as json in cells
            foreach ($data as $key => $value) {

                if (is_array($value) && $value != '[]') {
                    $data[$key] = json_encode($value);
                }
            }

        } else {
            if (!empty($data)) {
                $data = json_decode($data, true);
            } else {
                $data = '';
            }
        }

        return $this->db->insert($table, $data);
    }


    /**
     * @param $table
     * @param $data
     * @param int $where_id
     * @return bool
     */
    public function update($table, $data, $where_id = 0)
    {

        if (is_array($data)) {

            // Store arrays as json in cells
            foreach ($data as $key => $value) {

                if (is_array($value)) {
                    $data[$key] = json_encode([$value]);
                }
            }

        } else {
            $data = json_decode($data);
        }

        if (!empty($where_id)) {
            return $this->db->where('id', $where_id)->update($table, $data);
        }

        return false;

    }


    /**
     * @param $table
     * @param int $where_id
     * @return mixed
     */
    public function select($table, $where_id = 0)
    {
        if (empty($where_id)) {
            $q = $this->db->select('*')->from($table)->get();
        } else {
            $q = $this->db->select('*')->from($table)->where('id', $where_id)->get();
        }

        return $q->result_array();
    }



}