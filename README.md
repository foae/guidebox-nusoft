# Description #  
 
__This script makes delayed requests towards the Guidebox API__   
1. Stores the data (information) locally, broken down, into a MySQL database;  
2. Downloads all images and stores them locally;  
3. Maintains the local database updated using the Guidebox update API calls (cron).  
4. Maintains the local database updated up to 1000 movies and 100 shows (cron).  
  
## MySQL / MariaDB config ##
A minor tweak must be made to the mysql config file:  
* Find the innodb values in `/etc/mysql`  
* Add or edit with the following values:  
> innodb_log_file_size = 800M  
> innodb_log_buffer_size = 500M  
> innodb_file_format = Barracuda  

## Initial copy mechanism

- http://guidebox.plainsight.ro/store/movies
- http://guidebox.plainsight.ro/store/shows

## Cron jobs (sync) ##
**One** of the following tasks must be registered with cron:  
> Manual curl request: **`00 08,23 * * * curl http://guidebox.plainsight.ro/store/sync`**  
or  
> PHP script: **`00 08,23 * * * php /path/to/sync.php`**  
This will check for updated/modified/deleted movies, shows and episodes. __This must be set at a 12 hours interval to avoid collisions.__

## API v1 calls ##
1. http://guidebox.plainsight.ro/api/v1/movies.php - **?id=movie_id**
2. http://guidebox.plainsight.ro/api/v1/shows.php - **?id=show_id**
3. http://guidebox.plainsight.ro/api/v1/feed.php - **?type=movies&start=1&amount=20** or **?type=shows&start=18&amount=5**

## Path to script that updates Guidebox sort order ##
http://bundler.nusofthq.com/guidebox_rating.php

## Debugging ##
1. The whole script is very verbose. It will log any modified, deleted, processed and even shows/movies/episodes which are not in our local database.  
**Location: application/logs**