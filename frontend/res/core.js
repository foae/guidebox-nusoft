$(function(){
  //default to the movie feed page
  if(window.location.hash==""){
    window.location.hash="#feed_movies";
  }else{
    handleRoute();
  }
  //check page and render the correct page
  $(window).on('hashchange',function(){
    handleRoute();
  });

  //sources selection
  $(".side-menu").hide();
  $(".toggle-menu").click(function(){
    //show menu
    $(".side-menu").fadeToggle("slow");
    //disable scrolling
    if($("body").css("overflow")=="hidden"){
      //close sources panel
      $("body").css("overflow","visible");
      //re-render feed
      handleRoute();
    } else {
      //open sources panel
      $("body").css("overflow","hidden");
    }
  });
});

function handleRoute(){
  $("#container").empty();
  if(window.location.hash.indexOf("#feed_movies")!=-1 || window.location.hash=="") {
    //feed
    //add header
    var pageOffset = 0;
    $("#container").prepend("<div align='center' style='margin-top:10px;'><a href='#feed_movies' id='cat_movies' class='btn btn-default'>Movies</a> <a href='#feed_shows' id='cat_shows' class='btn btn-default'>Shows</a></div>");
    renderPage("feed_movies");
    handlePagination("feed_movies",pageOffset);
    $(".toggle-menu").show();

  }else if(window.location.hash.indexOf("#feed_shows")!=-1){
    //feed
    //add header
    var pageOffset = 0;
    $("#container").prepend("<div align='center' style='margin-top:10px;'><a href='#feed_movies' id='cat_movies' class='btn btn-default'>Movies</a> <a href='#feed_shows' id='cat_shows' class='btn btn-default'>Shows</a></div>");
    renderPage("feed_shows");
    handlePagination("feed_shows",pageOffset);
    $(".toggle-menu").show();
  }else if(window.location.hash.indexOf("movie")!=-1){
    //it's a movie
    var cat = "movie";
    var id = window.location.hash.replace("#movie","");
    renderPage(cat,id);
    $(window).unbind('scroll');
    $(".toggle-menu").hide();
  } else if(window.location.hash.indexOf("show")!=-1){
    //it's a show
    var cat = "show";
    var id = window.location.hash.replace("#show","");
    renderPage(cat,id)
    $(window).unbind('scroll');
    $(".toggle-menu").hide();
  } else {
    renderPage("error");
    $(window).unbind('scroll');
  }

}

function handlePagination(page,pageOffset){
  pageOffset = 0;
  $(window).on('scroll',function(){
    if(detectEndOfPage()){
      pageOffset+=10;
      renderPage(page,0,pageOffset);
    }
  });
}
