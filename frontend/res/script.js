var state = "page";
$(function(){
  //default to the movie feed page
  if(window.location.hash==""){
    window.location.hash="#feed_movies";
  }else{
    handleRoute();
  }
  //check page and render the correct page
  $(window).on('hashchange',function(){
    handleRoute();
  });
});

function handleRoute(){
  //detect if show or movie
  if(window.location.hash.indexOf("#feed_movies")!=-1 || window.location.hash=="") {
    //feed
    createFeedHTML('movies');
  }else if(window.location.hash.indexOf("#feed_shows")!=-1){
    createFeedHTML('shows');
  }else if(window.location.hash.indexOf("movie")!=-1){
    //it's a movie
    var cat = "movie";
    var id = window.location.hash.replace("#movie","");
    renderPage(cat,id)
  } else if(window.location.hash.indexOf("show")!=-1){
    //it's a show
    var cat = "show";
    var id = window.location.hash.replace("#show","");
    renderPage(cat,id)
  } else {
    showErrorPage();
  }
}

function renderPage(cat,id){
  state = "page";
  window.scrollTo(0,0);
  $(window).off('scroll');
  $.ajax({
    type:"GET",
    dataType:"json",
    url:"http://bundler.nusofthq.com/api/v1/"+cat+".php",
    data:"id="+id,
    beforeSend:function(xhr){
      $('body').append("<div id='loading_message'></div>").hide().fadeIn();
    },
    success:function(data){
      if(cat=="movie"){
        try{
          createMovieHTML(data);
        } catch(err){
          showErrorPage();
          console.error(err);
        }
      }else if(cat=="show"){
        try{
          createShowHTML(data);
        } catch(err){
          console.error(err);
        }
      }
    },
    error: function(xhr,status){
      console.warn("===============");
      console.warn(xhr);
      console.warn(status);
      console.warn("===============");
    },
    complete:function(){
      $("#loading_message").fadeOut(function(){$("#loading_message").remove()});
    }
  });
}

//generate html for movie page
function createMovieHTML(data){
  //set the page title to the movie title
  var title = data.basic_info.title;

  //check if poster is available
  if(data.images==false){
    var posterImage = "res/no_photo.png";
  } else {
    if(data.images.posters[0].large.url==null){
      console.warn(data);
    }
    var posterImage = "http://bundler.nusofthq.com/_img/"+data.images.posters[0].large.url;
  }

  //gather data
  var year = data.basic_info.release_year;
  var duration = formatTime(data.basic_info.duration);
  var rating = data.basic_info.rating;
  var summary = data.basic_info.overview;
  var cast_code = inlineListGen(data.cast.reverse());
  var directors_code = inlineListGen(data.directors);
  var writers_code = inlineListGen(data.writers);
  var sources = getMovieSources(data);

  //clear the entire page
  $("#container").empty();

  //add back button
  $("#container")
  .append("<div id='page'><a id='nav_back' href='#feed_movies'><i class='fa fa-arrow-left'></i></a></div>");

  //add all the elements
  $("#page")
  .append("<div id='movie_poster'></div>")
  .append("<div id='cont' style='padding:10px;'></div>");

  $("#cont")
  .append("<h1>"+title+" ("+year+")</h1>")
  .append("<small>"+duration+"</small><br>")
  .append("<small>Rating: <b>"+rating+"</b></small>")
  .append("<h2>Summary</h2>")
  .append("<p>"+summary+"</p>")
  .append("<h2>Cast</h2>")
  .append(cast_code)
  .append("<h2>Directors</h2>")
  .append(directors_code)
  .append("<h2>Writers</h2>")
  .append(writers_code)
  .append("<h2>Free</h2>")
  .append(sources['free'])
  .append("<h2>Purchase</h2>")
  .append(sources['purchase'])
  .append("<h2>Streaming</h2>")
  .append(sources['streaming'])
  .append("<h2>TV Everywhere</h2>")
  .append(sources['tv_evr']);

  //set poster image
  $("#movie_poster").css('background-image','url('+posterImage+')');

  //set page title
  document.title=title + " ("+year+")";
}

//generate html for show page
function createShowHTML(data){
  //console.log(data);

  //set the page title to the movie title
  var title = data.basic_info.title;
  var first_aired = data.basic_info.first_aired.split("-")[0];
  var status = data.basic_info.status;

  //set page title
  document.title=title;

  //check if poster is available
  if(data.images==false){
    var posterImage = "res/no_photo.png";
  } else {
    if(data.images.posters[0].large.url==null){
      console.warn(data);
    }
    var posterImage = "http://bundler.nusofthq.com/_img/"+data.images.posters[0].large.url;
  }

  //gather data
  var year = data.basic_info.release_year;
  var rating = data.basic_info.rating;
  var summary = data.basic_info.overview;
  var cast_code = inlineListGen(data.cast.reverse());
  var freeEpisodes = getFreeEpisodes(data.episodes);
  var episodes = getEpisodes(data.episodes.sort(function(a,b){ return a.season_number - b.season_number;}));//episodes[season].sort(function(a,b){ return a.episode_number - b.episode_number; });

  //console.log(freeEpisodes);
  //clear the entire page
  $("#container").empty();

  //add back button
  $("#container")
  .append("<div id='page'><a id='nav_back' href='#feed_shows'><i class='fa fa-arrow-left'></i></a></div>");

  //add all the elements
  $("#page")
  .append("<div id='movie_poster'></div>")
  .append("<div id='cont' style='padding:10px;'></div>");

  $("#cont")
  .append("<h1>"+title+"</h1>")
  .append("<small>Rating: <b>"+rating+"</b></small>")
  .append("<h2>Summary</h2>")
  .append("<p>"+summary+"</p>")
  .append("<h2>Cast</h2>")
  .append(cast_code)
  .append("<h2>View episodes</h2>")
  .append("<h3>Free Episodes</h3>")
  .append(freeEpisodes)
  .append(episodes)

  //set poster image
  $("#movie_poster").css('background-image','url('+posterImage+')');

  //get episodes
  function getEpisodes(data){
    var res;
    var episodes = []
    data.forEach(function(episode){
      var seasonForThisEpisode = episode.season_number;
      var slug = String("season_"+seasonForThisEpisode);
      if(typeof episodes[slug]==='undefined'){
        eval("episodes['"+slug+"'] = Array();")
        eval("episodes['"+slug+"'].push(episode);");
      } else {
        eval("episodes['"+slug+"'].push(episode);");
      }
    });

    res = "<table class='table'>";
    for(season in episodes){
      //format season label
      var fSeason = season.split("_");
      if(fSeason[1]=="0"){
        //special episodes are stored as being part of season 0
        res+="<tr><td colspan='3' style='text-transform:capitalize'><h3>Specials</h3></td></tr>";
      } else {
        res+="<tr><td colspan='3' style='text-transform:capitalize'><h3>"+fSeason[0]+" "+fSeason[1]+"</h3></td></tr>";
      }

      //sorting episodes by episode number
      episodes[season].sort(function(a,b){ return a.episode_number - b.episode_number; });

      for(episode in episodes[season]){
        var thisEpisode = episodes[season][episode];
        var episode_title = thisEpisode.original_title;
        var episode_number = ((thisEpisode.episode_number=="0") ? "" : thisEpisode.episode_number+". ");
        var episode_slug = episode_title.replaceAll(" ","-");
        var episode_sources = getEpisodeSources(thisEpisode);
        res+="<tr align='center'><td id='"+episode_slug+"'><h4>"+episode_number+episode_title+"</h3><br>"+episode_sources+"</td></tr>";
      }
    }

    res+="</table>";
    return res;
  }

  function getFreeEpisodes(data){
    var res ="";
    var episodes = [];
    res+="<table class='table'>";
    data.forEach(function(episode){
      if(episode.sources_free_web.length>0){
        var freeSources=Array();
        episode.sources_free_web.forEach(function(free){
          freeSources.push("<a href='"+free.link+"'>"+free.display_name+"</a>");
        });
        freeSources.sort(function(a,b){return a.episode_nubmer - b.episode_number;});

        var epInfo = (episode.season_number!=0) ? "(S "+episode.season_number+" E"+episode.episode_number+")":"";

        res+="<tr align='center'><td><h4>"+ epInfo +" "+episode.original_title+"</h4></td></tr>";
        freeSources.forEach(function(j){
          res+="<tr align='center'><td>"+j+"</td></tr>";
        });
      }
    });
    if(res==="<table class='table'>"){
      res+="<tr align='center'><td>No free episodes available</td></tr>";
      res+="</table>";
      return res;
    }else {
      res+="</table>";
      return res;
    }

  }
}

//generate html for main feed page
function createFeedHTML(type){
  $(window).off('scroll');
  document.title="Guidebox Mirror";
  if(type=="movies"){
    $("#cat_movies").addClass('active');
  } else if(type=="shows"){
    $("#cat_shows").addClass('active');
  }
  $("#container").empty();
  $("#container").append("<div align='center' style='margin-top:10px;'><a href='#feed_movies' id='cat_movies' class='btn btn-default'>Movies</a> <a href='#feed_shows' id='cat_shows' class='btn btn-default'>Shows</a></div>")
  var feed = [];
  var pageOffset = 0;

  $('body').append("<div id='loading_message'></div>").hide().fadeIn();
  makeFeedAjaxReq(pageOffset,type);
  $("#loading_message").fadeOut(function(){$("#loading_message").remove()});
  $(window).on('scroll',function(){
    var iseop = detectEndOfPage();
    if(iseop){
      //
      pageOffset+=10;
      $("#loading").fadeOut();
      makeFeedAjaxReq(pageOffset,type);
    }
  });


  function makeFeedAjaxReq(offset,type){
    console.log(type);

    if(type=="movies"){
      $.ajax({
        type:"GET",
        dataType:"json",
        url:"http://bundler.nusofthq.com/api/v1/feed.php",
        data:{"start":offset,"popular":"true"},
        success:function(data){
          // $("#loading").remove();
          document.title="Guidebox Mirror | Movies";
          //
          data.forEach(function(i){
            var title = i.original_title;
            var image = ((i.posters!==false) ? "http://bundler.nusofthq.com/_img/"+i.posters.posters[0].small.url : "res/no_photo.png");
            var release_year = i.release_year;
            var duration = formatTime(i.duration);
            var rating = i.rating;
            var code = "<p><img src='"+image+"' style='float:left;width:120px;height:171px;margin-right:10px;'/><b>"+title+"</b></p><p>"+release_year+" / "+rating+"</p>"+duration;
            $("#container").append("<a class='thumbnail' href='#movie"+i.id+"'>"+code+"</a>");
          });
          $("#loading").remove();
          $("#container").append("<div id='loading'><img src='res/loading.gif' width=20/><br>load more</div>");
        }
      });
    } else if(type=="shows"){
      //shows
      $.ajax({
        type:"GET",
        dataType:"json",
        url:"http://bundler.nusofthq.com/api/v1/feed.php",
        data:{"type":"shows","start":offset,"popular":"true"},//"type=shows&start="+offset,
        success:function(data){
          document.title="Guidebox Mirror | Shows";

          data.forEach(function(i){
            var title = i.title;
            var image = "http://bundler.nusofthq.com/_img/"+i.posters.posters[0].small.url;
            var release_year = i.release_year;
            var duration = formatTime(i.runtime*60);
            var rating = i.rating;
            var code = "<p><img src='"+image+"' style='float:left;width:120px;height:171px;margin-right:10px;'/><b>"+title+"</b></p><p>"+rating+"</p>"+duration;
            $("#container").append("<a class='thumbnail' href='#show"+i.id+"'>"+code+"</a>");
          });
          $("#loading").remove();
          $("#container").append("<div id='loading'><img src='res/loading.gif' width=20/><br>load more</div>");
        }
      });
    }
  }


}


//generate html for error page
function showErrorPage(){
  $("#container").empty();
  $("#container")
  .append("<h1>Error</h1>");
}


//various
function detectEndOfPage(){
  if($(window).scrollTop() + window.innerHeight == $(document).height()) {
    state = "feed";
    return true;
  }
}

function formatTime(time){
  var numhours = Math.floor(((time % 31536000) % 86400) / 3600);
  var numminutes = Math.floor((((time % 31536000) % 86400) % 3600) / 60);
  var numseconds = (((time % 31536000) % 86400) % 3600) % 60;

  var hourText = "hour";
  if(numhours>1){
    hourText = "hours";
  }

  var minuteText = "minutes";
  if(numminutes==1){
    minuteText = "minute"
  }

  if(numminutes==0){
    return numhours+" "+hourText;
  } else if (numhours==0){
    return numminutes+" "+minuteText;
  } else {
    return numhours+" "+hourText+" and "+numminutes+" "+minuteText;
  }


}

function inlineListGen(data){
  var code = "<div class='inline-list' align='left'><ul>";
  data.forEach(function(i){
    if(i.images==""){
      imagePath="res/no_photo.png";
    }else{
      imagePath="http://bundler.nusofthq.com/_img/"+i.images;
    }
    // code+="<li><div align='center'><img src='"+imagePath+"' width='40'class='img-circle'/><br><span>"+i.name+"</span></div></li>";
    code+="<li><div align='center'><div class='small-thumbnail' style='background-image:url("+imagePath+");' align='center'></div><br><span>"+i.name+"</div></li>";
  });

  code+="</ul>";

  return code;
}

function getMovieSources(data){

  var code = Array();
  var res_free = {web:data.sources_free_web, ios:data.sources_free_ios,android:data.sources_free_android};
  var res_purchase = {web: data.sources_purchase_web, ios: data.sources_purchase_ios, android:data.sources_purchase_android}
  var res_streaming = {web: data.sources_subscription_web, ios: data.sources_subscription_ios, android:data.sources_subscription_android}
  var res_tvevr = {web: data.sources_tv_everywhere_web, android: data.sources_tv_everywhere_android, ios: data.sources_tv_everywhere_ios}

  code['free']=generateFreeTable(res_free);
  code['purchase']=generatePurchaseTable(res_purchase);
  code['streaming']=generateSubscriptionTable(res_streaming);
  code['tv_evr']=generateTvTable(res_tvevr);

  function generatePurchaseTable(data){
    //table head
    res="<table class='table'>";
    for(var key in data){

      //format platform name
      if(key=="ios") { key_f="iOS"; } else if(key=="android"){ key_f="Android"; } else if(key=="web") { key_f="Web"; }

      //add platform name
      res+="<tr><td colspan='3'><h3>"+key_f+"</h3></td></tr>";

      //show all sources for each platform
      //check if there are any links available
      if(data[key].length>0){
        data[key].forEach(function(i){
          var source_formats = JSON.parse(i.formats);
          var hd_prc, sd_prc,hd_rent,sd_rent;
          var str = {
            source_name:i.display_name,
            source_link:i.link,
            prc_hd:"",
            prc_sd:"",
            rent_hd:"",
            rent_sd:""
          };

          source_formats.forEach(function(formats){
            if(formats.format=="SD" && formats.type=="purchase"){
              str.prc_sd="Purchase SD ($"+formats.price+")";
            } else if (formats.format=="HD" && formats.type=="purchase"){
              str.prc_hd="Purchase HD ($"+formats.price+")";
            } else if(formats.format=="SD" && formats.type=="rent"){
              str.rent_sd="Rent SD ($"+formats.price+")";
            } else if (formats.format=="HD" && formats.type=="rent"){
              str.rent_hd="Rent HD ($"+formats.price+")";
            }
          });
          res+="<tr><td><a href='"+str.source_link+"'>"+str.source_name+"</a></td><td>"+str.prc_hd+"<br>"+str.rent_hd+"</td><td>"+str.prc_sd+"<br>"+str.rent_sd+"</td></tr>";

        });
      } else {
        //no links available
        res+="<tr><td colspan='3' align='center'>No results</td></tr>";
      }
    }
    //end table
    res+="</table>";
    //return html code
    return res;
  }

  function generateSubscriptionTable(data){
    //table head
    res="<table class='table'>";
    for(var key in data){

      //format platform name
      if(key=="ios") { key_f="iOS"; } else if(key=="android"){ key_f="Android"; } else if(key=="web") { key_f="Web"; }

      //add platform name
      res+="<tr><td colspan='3'><h3>"+key_f+"</h3></td></tr>";

      //show all sources for each platform
      //check if there are any links available
      if(data[key].length>0){
        data[key].forEach(function(i){
          var source_formats = JSON.parse(i.formats);
          var hd_prc, sd_prc,hd_rent,sd_rent;
          var str = {
            source_name:i.display_name,
            source_link:i.link,
          };

          res+="<tr><td colspan='3' align='center'><a href='"+str.source_link+"'>"+str.source_name+"</a></tr>";

        });
      } else {
        //no links available
        res+="<tr><td colspan='3' align='center'>No results</td></tr>";
      }
    }
    //end table
    res+="</table>";
    //return html code
    return res;
  }

  function generateTvTable(data){
    res="<table class='table'>";
    for(var key in data){
      //format platform name
      if(key=="ios") { key_f="iOS"; } else if(key=="android"){ key_f="Android"; } else if(key=="web") { key_f="Web"; }

      res+="<tr><td colspan='3'><h3>"+key_f+"</h3></td></tr>";

      //show all sources for each platform
      //check if there are any links available
      if(data[key].length>0){
        data[key].forEach(function(i){
          var source_formats = JSON.parse(i.formats);
          var hd_prc, sd_prc,hd_rent,sd_rent;
          var str = {
            source_name:i.display_name,
            source_link:i.link,
          };

          res+="<tr><td colspan='3' align='center'><a href='"+str.source_link+"'>"+str.source_name+"</a></tr>";

        });
      } else {
        //no links available
        res+="<tr><td colspan='3' align='center'>No results</td></tr>";
      }
    }
    //end table
    res+="</table>";
    return res;
  }

  function generateFreeTable(data){
    var res = "";
    res+="<table class='table'>";
    res+="<tr><td><h4>Web</h4>";
    if(res_free.web.length!=0){
      res_free.web.forEach(function(i){
        res+="<a href='"+i.link+"'>"+i.display_name+"</a>";
      });
    } else {
      res+="<i>No links available</i>";
    }
    res+="</td></tr>";
    res+="<tr><td><h4>iOS</h4>";
    if(res_free.web.length!=0){
      res_free.ios.forEach(function(i){
        res+="<a href='"+i.link+"'>"+i.display_name+"</a>";
      });
    } else {
      res+="<i>No links available</i>";
    }
    res+="</td></tr>";
    res+="<tr><td><h4>Android</h4>";
    if(res_free.android.length!=0){
      res_free.android.forEach(function(i){
        res+="<a href='"+i.link+"'>"+i.display_name+"</a>";
      });
    }else {
      res+="<i>No links available</i>";
    }

    res+="</td></tr>";
    //res+="<tr>"+res_free.web+"</tr>";
    res+="</table>";
    return res;
  }
  return code;
}

function getEpisodeSources(data){
  var res;

  var sourcesAndroid = getSourcesAndroid(data);
  var sourcesIOS = getSourcesIOS(data);
  var sourcesWeb = getSourcesWeb(data);
  var streamingAndroid = getStreamingAndroid(data);
  var streamingios = getStreamingIos(data);
  var streamingWeb = getStreamingWeb(data);


  function getSourcesAndroid(sources){
    var res = Array();
    sources.sources_purchase_android.forEach(function(i){
      res.push(i)
    });
    return res;
  }
  function getSourcesIOS(sources){
    var res = Array();
    sources.sources_purchase_ios.forEach(function(i){
      res.push(i)
    });
    return res;
  }
  function getSourcesWeb(sources){
    var res = Array();
    sources.sources_purchase_web.forEach(function(i){
      res.push(i)
    });
    return res;
  }
  function getStreamingAndroid(sources){
    var res = Array();
    sources.sources_subscription_android.forEach(function(i){
      res.push(i)
    });
    return res;
  }
  function getStreamingIos(sources){
    var res = Array();
    sources.sources_subscription_ios.forEach(function(i){
      res.push(i)
    });
    return res;
  }
  function getStreamingWeb(sources){
    var res = Array();
    sources.sources_subscription_web.forEach(function(i){
      res.push(i)
    });
    return res;
  }
  res = "<div class='row'><h5 align='left'>Purchase</h5>";

  res+= "<div class='col-xs-4'>";
  res+= "<p>Android</p>";
  res+= "<ul>";
  sourcesAndroid.forEach(function(i){
    var parseFormats = JSON.parse(i.formats);
    var formats="";
    var constructFormats = parseFormats.forEach(function(j){
      formats+=" <b>"+j.format+":</b> $"+j.price+"";
    });
    res+="<a href='"+i.link+"'>"+i.app_name+"</a>"+formats+"<br>";
  });
  res+="</ul>";
  res+="</div><!-- end col-->";

  res+= "<div class='col-xs-4'>";
  res+= "<p>iOS</p>";
  res+= "<ul>";
  sourcesIOS.forEach(function(i){
    var parseFormats = JSON.parse(i.formats);
    var formats="";
    var constructFormats = parseFormats.forEach(function(j){
      formats+=" <b>"+j.format+":</b> $"+j.price+" ";
    });
    res+="<a href='"+i.link+"'>"+i.app_name+"</a>"+formats+"<br>";
  });
  res+="</ul>";
  res+="</div><!-- end col-->";

  res+= "<div class='col-xs-4'>";
  res+= "<p>Web</p>";
  res+= "<ul>";
  sourcesWeb.forEach(function(i){
    var parseFormats = JSON.parse(i.formats);
    var formats="";
    var constructFormats = parseFormats.forEach(function(j){
      formats+=" <b>"+j.format+":</b> $"+j.price+" ";
    });
    res+="<a href='"+i.link+"'>"+i.display_name+"</a>"+formats+"<br>";
  });
  res+="</ul>";
  res+="</div><!-- end col-->";

  res+="</div>";

  res+="<div class='row'><h5 align='left'>Stream</h5>";
  res+="<div class='col-xs-4'>";
  res+="<p>Android</p>";
  if(streamingAndroid.length!=0){
    streamingAndroid.forEach(function(i){
      // var appName = JSON.parse(i.app_name);
      // var formats="";
      // var constructFormats = parseFormats.forEach(function(j){
      //   formats+=" <b>"+j.format+":</b> $"+j.price+" ";
      // });
      res+="<a href='"+i.link+"'>"+i.display_name+"</a><br>";
    });
  } else{
    res+="<i>No links available</i>";
  }
  res+="</div>";

  res+="<div class='col-xs-4'>";
  res+="<p>iOS</p>";
  if(streamingios.length!=0){
    streamingios.forEach(function(i){
      // var appName = JSON.parse(i.app_name);
      // var formats="";
      // var constructFormats = parseFormats.forEach(function(j){
      //   formats+=" <b>"+j.format+":</b> $"+j.price+" ";
      // });
      res+="<a href='"+i.link+"'>"+i.display_name+"</a><br>";
    });
  } else{
    res+="<i>No links available</i>";
  }
  res+="</div>";

  res+="<div class='col-xs-4'>";
  res+="<p>Web</p>";
  if(streamingWeb.length!=0){
    streamingWeb.forEach(function(i){
      // var appName = JSON.parse(i.app_name);
      // var formats="";
      // var constructFormats = parseFormats.forEach(function(j){
      //   formats+=" <b>"+j.format+":</b> $"+j.price+" ";
      // });
      res+="<a href='"+i.link+"'>"+i.display_name+"</a><br>";
    });
  } else{
    res+="<i>No links available</i>";
  }

  res+="</div>";

  res+="</div>";
  return res;
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
