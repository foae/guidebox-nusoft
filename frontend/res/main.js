function renderPage(pageType,id,offset){
  id = id || 0;
  offset = offset || 0;
  //get filter preferences
  var local = JSON.parse(window.localStorage.getItem("bundler_sources"));
  var filter;
  if(local==null){
    console.warn("main.js - no preferences");
    filter = "";
  } else {
    var res = [];
    for(i in local){
      if(local[i]['enabled']==true){
        res.push(i);
      }
    }
    filter = res.join(",");
  }
  switch(pageType){
    case "feed_movies":
      $.ajax({
        url:"http://bundler.nusofthq.com/api/v1/feed.php",
        processData:false,
        data:"start="+offset+"&popular=true&filter="+filter,//{"start":offset,"popular":"true","filter":decodeURI(filter)},
        success:function(data){ $("#loading").remove();processTemplate({"response":data}); }
      });
      break;
    case "feed_shows":
      $.ajax({
        url:"http://bundler.nusofthq.com/api/v1/feed.php",
        processData:false,
        data:"type=shows&start="+offset+"&popular=true&filter="+filter,
        success:function(data){ $("#loading").remove();processTemplate({"response":data}); }
      });
      break;
    case "movie":
      $.ajax({
        url:"http://bundler.nusofthq.com/api/v1/movie.php",
        data:{"id":id},
        beforeSend:function(){
          $('body').append("<div id='loading_message'></div>").hide().fadeIn();
        },
        success:function(data){processTemplate(data)}
      });
      break;
    case "show":
      $.ajax({
        url:"http://bundler.nusofthq.com/api/v1/show.php",
        data:{"id":id},
        beforeSend:function(){
          $('body').append("<div id='loading_message'></div>").hide().fadeIn();
        },
        success:function(data){processTemplate(data)}
      });
      break;
  }
  function processTemplate(pageData){
    $.get("res/templates/"+pageType+".html",function(data){
      var templateCompile = Handlebars.compile(data);
      var code = templateCompile(pageData);
      $("#container").append(code);
      $("#loading_message").fadeOut(function(){$("#loading_message").remove();});
    });
  }

}
