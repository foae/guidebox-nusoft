/*
 *  Generate inline list with scrolling. Used when displaying persons
 */
Handlebars.registerHelper("inlineListGen",function(data){
  var code = "<div class='inline-list' align='left'><ul>";
  data.reverse().forEach(function(i){
    if(i.images==""){
      imagePath="res/no_photo.png";
    }else{
      imagePath="http://bundler.nusofthq.com/_img/"+i.images;
    }
    // code+="<li><div align='center'><img src='"+imagePath+"' width='40'class='img-circle'/><br><span>"+i.name+"</span></div></li>";
    code+="<li><div align='center'><div class='small-thumbnail' style='background-image:url("+imagePath+");' align='center'></div><br><span>"+i.name+"</div></li>";
  });

  code+="</ul>";

  return new Handlebars.SafeString(code);
});

/*
 *  Generate free sources table (movies)
 */
Handlebars.registerHelper('freeSources',function(sources){
  var code = Array();
  var res ="";
  for(var platform in sources.hash){
    code[platform]=sources.hash[platform];
  }

  for(var platform in code){
    res+="<h4>"+formatPlatform(platform)+"</h4>";
    for(var i in code[platform]){
      // console.log(platform,i);
      if(i>0){
        res+="<i>No links available</i>";
      } else {
        res+="<a href='"+code[platform][i].link+"'>"+code[platform][i].display_name+"</a><br>";
      }

    }
  }
  return new Handlebars.SafeString(res);
});

/*
 *  Generate purchase sources table (movies)
 */
Handlebars.registerHelper('purchaseSources',function(sources){
  var code = Array();
  var res ="";
  for(var platform in sources.hash){
    code[platform]=sources.hash[platform];
  }
  // console.log(code);

  for(var platform in code){
    res+="<h4 align='center'>"+formatPlatform(platform)+"</h4><hr>";
    for(var i in code[platform]){
      var parseFormats = JSON.parse(code[platform][i].formats);
      var str = {source_name:code[platform][i].display_name,source_link:code[platform][i].link,prc_hd:"",prc_sd:"",rent_hd:"",rent_sd:""};
      parseFormats.forEach(function(formats){
        if(formats.format=="SD" && formats.type=="purchase"){
          str.prc_sd="Purchase SD ($"+formats.price+")";
        } else if (formats.format=="HD" && formats.type=="purchase"){
          str.prc_hd="Purchase HD ($"+formats.price+")";
        } else if(formats.format=="SD" && formats.type=="rent"){
          str.rent_sd="Rent SD ($"+formats.price+")";
        } else if (formats.format=="HD" && formats.type=="rent"){
          str.rent_hd="Rent HD ($"+formats.price+")";
        }
      });
      res+="<div class='row'>";
      res+="<div class='col-xs-4'><a href='"+str.source_link+"'>"+str.source_name+"</a></div>";
      res+="<div class='col-xs-4'>"+str.prc_hd+"<br>"+str.rent_hd+"</div>";
      res+="<div class='col-xs-4'>"+str.prc_sd+"<br>"+str.rent_sd+"</div>";
      res+="</div>";
      res+="<br>";
    }
  }
  return new Handlebars.SafeString(res);
});
/*
 *  Generate streaming sources table (movies)
 */
Handlebars.registerHelper('streamingSources',function(sources){
  var code = Array();
  var res ="";
  for(var platform in sources.hash){
    code[platform]=sources.hash[platform];
  }
  // console.log(code);

  for(var platform in code){
    res+="<h4 align='center'>"+formatPlatform(platform)+"</h4><hr>";
    for(var i in code[platform]){
      var str = {source_name:code[platform][i].display_name,source_link:code[platform][i].link};
      res+="<div class='row'>";
      res+="<div class='col-xs-12' align='center'><a href="+str.source_link+">"+str.source_name+"</a></div>";
      res+="</div>";
      res+="<br>";
    }
  }
  return new Handlebars.SafeString(res);
});

/*
 *  Generate TV Everywhere sources table (movies)
 */
Handlebars.registerHelper('tveverywhereSources',function(sources){
  var code = Array();
  var res ="";
  for(var platform in sources.hash){
    code[platform]=sources.hash[platform];
  }

  for(var platform in code){
    res+="<h4 align='center'>"+formatPlatform(platform)+"</h4><hr>";
    if(code[platform].length!=0){
      for(var i in code[platform]){
        var str = {source_name:code[platform][i].display_name,source_link:code[platform][i].link};
        res+="<div class='row'>";
        res+="<div class='col-xs-12' align='center'><a href="+str.source_link+">"+str.source_name+"</a></div>";
        res+="</div>";
        res+="<br>";
      }
    } else {
      res+="<div class='row'><div class='col-xs-12' align='center'>No Links Available</div></div>";
      res+="<br>";
    }

  }
  return new Handlebars.SafeString(res);
});




/*
 * Generate all sources
 */
Handlebars.registerHelper('episodeSources',function(sources){
  var episodes = sources.hash.episodes.sort(function(a,b){ return a.season_number - b.season_number; });
  var res = "";

  //parse each season and its episodes
  var episodesPerSeason = Array();
  episodes.forEach(function(episode){
    var seasonForThisEpisode = episode.season_number;
    var slug = String("season_"+seasonForThisEpisode);
    if(typeof episodesPerSeason[slug]==='undefined'){
      eval("episodesPerSeason['"+slug+"'] = Array();")
      eval("episodesPerSeason['"+slug+"'].push(episode);");
    } else {
      eval("episodesPerSeason['"+slug+"'].push(episode);");
    }
  });

  for(season in episodesPerSeason){
    res+=""
    var sorted_episodes = episodesPerSeason[season].sort(function(a,b){ return a.episode_number - b.episode_number; });
    var fSeason = season.split("_");
    if(fSeason[1]=="0"){
      //special episodes are stored as being part of season 0
      res+="<h3 style='text-transform:capitalize'>Specials</h3>";
    } else {
      res+="<h3 style='text-transform:capitalize'>"+fSeason[0]+" "+fSeason[1]+"</h3>";
    }
    episodesPerSeason[season].forEach(function(ep){
      res+="<h4>E "+ep.episode_number+" "+ep.original_title+"</h4>";
      res+=thisEpisodeSources(ep);
      res+="<hr>";
    });
  }
  episodesPerSeason.forEach(function(season){
    console.log(episodesPerSeason);

  });

  return new Handlebars.SafeString(res);

  function thisEpisodeSources(episodeData){
    var data = episodeData;

    var sourcesAndroid = getSourcesAndroid(data);
    var sourcesIOS = getSourcesIOS(data);
    var sourcesWeb = getSourcesWeb(data);
    var streamingAndroid = getStreamingAndroid(data);
    var streamingios = getStreamingIos(data);
    var streamingWeb = getStreamingWeb(data);


    function getSourcesAndroid(sources){
      var res = Array();
      // console.log(sources);
      sources.sources_purchase_android.forEach(function(i){
        res.push(i)
      });
      return res;
    }
    function getSourcesIOS(sources){
      var res = Array();
      sources.sources_purchase_ios.forEach(function(i){
        res.push(i)
      });
      return res;
    }
    function getSourcesWeb(sources){
      var res = Array();
      sources.sources_purchase_web.forEach(function(i){
        res.push(i)
      });
      return res;
    }
    function getStreamingAndroid(sources){
      var res = Array();
      sources.sources_subscription_android.forEach(function(i){
        res.push(i)
      });
      return res;
    }
    function getStreamingIos(sources){
      var res = Array();
      sources.sources_subscription_ios.forEach(function(i){
        res.push(i)
      });
      return res;
    }
    function getStreamingWeb(sources){
      var res = Array();
      sources.sources_subscription_web.forEach(function(i){
        res.push(i)
      });
      return res;
    }
    res = "<div class='row'><h5 align='center'>Purchase</h5>";

    res+= "<div class='col-xs-4'>";
    res+= "<p>Android</p>";
    res+= "<ul>";
    sourcesAndroid.forEach(function(i){
      var parseFormats = JSON.parse(i.formats);
      var formats="";
      var constructFormats = parseFormats.forEach(function(j){
        formats+=" <b>"+j.format+":</b> $"+j.price+"";
      });
      res+="<a href='"+i.link+"'>"+i.app_name+"</a>"+formats+"<br>";
    });
    res+="</ul>";
    res+="</div><!-- end col-->";

    res+= "<div class='col-xs-4'>";
    res+= "<p>iOS</p>";
    res+= "<ul>";
    sourcesIOS.forEach(function(i){
      var parseFormats = JSON.parse(i.formats);
      var formats="";
      var constructFormats = parseFormats.forEach(function(j){
        formats+=" <b>"+j.format+":</b> $"+j.price+" ";
      });
      res+="<a href='"+i.link+"'>"+i.app_name+"</a>"+formats+"<br>";
    });
    res+="</ul>";
    res+="</div><!-- end col-->";

    res+= "<div class='col-xs-4'>";
    res+= "<p>Web</p>";
    res+= "<ul>";
    sourcesWeb.forEach(function(i){
      var parseFormats = JSON.parse(i.formats);
      var formats="";
      var constructFormats = parseFormats.forEach(function(j){
        formats+=" <b>"+j.format+":</b> $"+j.price+" ";
      });
      res+="<a href='"+i.link+"'>"+i.display_name+"</a>"+formats+"<br>";
    });
    res+="</ul>";
    res+="</div><!-- end col-->";

    res+="</div>";

    res+="<div class='row'><h5 align='center'>Stream</h5>";
    res+="<div class='col-xs-4'>";
    res+="<p>Android</p>";
    if(streamingAndroid.length!=0){
      streamingAndroid.forEach(function(i){
        // var appName = JSON.parse(i.app_name);
        // var formats="";
        // var constructFormats = parseFormats.forEach(function(j){
        //   formats+=" <b>"+j.format+":</b> $"+j.price+" ";
        // });
        res+="<a href='"+i.link+"'>"+i.display_name+"</a><br>";
      });
    } else{
      res+="<i>No links available</i>";
    }
    res+="</div>";

    res+="<div class='col-xs-4'>";
    res+="<p>iOS</p>";
    if(streamingios.length!=0){
      streamingios.forEach(function(i){
        // var appName = JSON.parse(i.app_name);
        // var formats="";
        // var constructFormats = parseFormats.forEach(function(j){
        //   formats+=" <b>"+j.format+":</b> $"+j.price+" ";
        // });
        res+="<a href='"+i.link+"'>"+i.display_name+"</a><br>";
      });
    } else{
      res+="<i>No links available</i>";
    }
    res+="</div>";

    res+="<div class='col-xs-4'>";
    res+="<p>Web</p>";
    if(streamingWeb.length!=0){
      streamingWeb.forEach(function(i){
        // var appName = JSON.parse(i.app_name);
        // var formats="";
        // var constructFormats = parseFormats.forEach(function(j){
        //   formats+=" <b>"+j.format+":</b> $"+j.price+" ";
        // });
        res+="<a href='"+i.link+"'>"+i.display_name+"</a><br>";
      });
    } else{
      res+="<i>No links available</i>";
    }

    res+="</div>";

    res+="</div>";
    return res;
  }
});
/*
 *  Generate Free Episode Sources (shows)
 */
 Handlebars.registerHelper('freeEpisodeSources',function(sources){
   var code = Array();
   var res="";
   var type = sources.hash.type;

   sources.hash.episodes.forEach(function(episode){
    if(episode.sources_free_web.length>0 || episode.sources_free_android.length >0 || episode.sources_free_ios.length>0){
      code.push(episode);
    }
   });
   code.forEach(function(episode){
     res+=episodeInfo(episode);
   });
   return new Handlebars.SafeString(res);

   //various
   function episodeInfo(data){
     res="<h3>(S "+data.season_number+" E "+data.episode_number+") "+data.title+"</h3>";
     res+="<div class='row'>";
     res+="<div class='col-xs-4'>Web<br>";
     res+=parseSources(data.sources_free_web);
     res+="</div>";

     res+="<div class='col-xs-4'>iOS<br>";
     res+=parseSources(data.sources_free_ios);
     res+="</div>";

     res+="<div class='col-xs-4'>Android<br>";
     res+=parseSources(data.sources_free_android);
     res+="</div>";

     res+="</div>";
     return res;

     function parseSources(data){
       var r = "";
       if(data.length>0){
         data.forEach(function(i){
           r+="<a href='"+i.link+"'>"+i.display_name+"</a><br>";
         });
       } else {
         r="<i>No links available</i>";
       }

       return r;
     }
   }
 });











/*
 * Format platform name
 */
function formatPlatform(slug){
  if(slug=="ios"){ return "iOS"; }
  else if(slug=="android"){ return "Android";}
  else if(slug=="web"){ return "Web";}
}
/*
 * Convert time from seconds to HR time
 */
Handlebars.registerHelper("formatTime",function(time){
  var numhours = Math.floor(((time % 31536000) % 86400) / 3600);
  var numminutes = Math.floor((((time % 31536000) % 86400) % 3600) / 60);
  var numseconds = (((time % 31536000) % 86400) % 3600) % 60;

  var hourText = "hour";
  if(numhours>1){
    hourText = "hours";
  }

  var minuteText = "minutes";
  if(numminutes==1){
    minuteText = "minute"
  }

  if(numminutes==0){
    return numhours+" "+hourText;
  } else if (numhours==0){
    return numminutes+" "+minuteText;
  } else {
    return numhours+" "+hourText+" and "+numminutes+" "+minuteText;
  }
});
/*
 *  Detect end of page
 */
function detectEndOfPage(){
  if($(window).scrollTop() + window.innerHeight == $(document).height()) {
    state = "feed";
    return true;
  }
}
