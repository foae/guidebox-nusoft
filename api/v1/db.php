<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'guidebox');
define('DB_PASS', 'Angelina03');
// define('DB_PASS', 'guidebox');
define('DB_NAME', 'guidebox');
define('DEBUG', true);

if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}


/**
 * Class Database
 */
class Database
{

    /**
     * Holds the PDO object
     * @var object
     */
    public $pdo;

    /**
     * Holder for any error
     * @var array
     */
    public $error = [];

    /**
     * El Constructor
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * Given the credentials, attempt to connect to the DB
     */
    private function connect()
    {
        try {
            $this->pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo die($e->getMessage());
        }
    }

    /**
     * Executes a query; true on success, false on failure
     * @param $sql
     * @param array $data
     * @return mixed
     */
    public function execute($sql, array $data)
    {
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute($data);
    }


    /**
     * @param $sql
     * @param string $type
     * @return mixed
     */
    public function query($sql, $type = 'fetch')
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        if ($type == 'fetchAllRows') {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Select on DB with specific parameters
     * @param $tableName
     * @param string $whereItem
     * @param $whereValue
     * @return mixed
     */
    public function select($tableName, $whereItem = '', $whereValue)
    {
        if (!empty($whereItem)) {
            $query = "SELECT * FROM {$this->pdo->quote($tableName)} WHERE {$this->pdo->quote($whereItem)} = ':value'";
        } else {
            $query = "SELECT * FROM {$this->pdo->quote($tableName)}";
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(":value", $whereValue);

        return $stmt->fetchAll();
    }

    /**
     * Select on DB - fetch all
     * @param $tableName
     * @return mixed
     */
    public function selectAll($tableName)
    {
        return $this->pdo->query("SELECT * FROM {$this->pdo->quote($tableName)}");
    }

    /**
     * Insert a single item into the DB
     * @param $tableName
     * @param array $value
     * @return mixed
     */
    public function insert($tableName, array $value)
    {
        $stmt = $this->pdo->prepare("INSERT INTO $this->pdo->quote($tableName) VALUES (:name)");
        $stmt->execute([
            ':name' => $value
        ]);

        return $stmt->rowCount();
    }

    /**
     * Update method
     * @param $tableName
     * @param array $fields
     */
    public function update($tableName, array $fields)
    {
        $stmt = $this->pdo->prepare("UPDATE {$this->pdo->quote($tableName)} SET name = :name WHERE id = :id");
        $stmt->execute(array(
            ':id'   => $fields['id'],
            ':name' => $fields['name']
        ));

        echo $stmt->rowCount();
    }


    // Run manual queries. To be removed
    public function q($query) {
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute()->fetchAll();
    }

}



$db = new Database();
