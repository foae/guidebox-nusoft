<?php

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require_once 'db.php';
require_once 'helper.php';

$_type = 'movies';
$_img_table = 'movies_images';
$_table_id = 'movie_id';

$start = (int) (!empty($_GET['start'])) ? $_GET['start'] : 0;
$amount = (int) (!empty($_GET['amount'])) ? $_GET['amount'] : 10;
$orderBy = (!empty($_GET['popular']) && $_GET['popular']=='true') ? 'guidebox_rating' : 'timestamp';
$sources = (empty($_GET['filter'])) ? 'all' : $_GET['filter'];

function constructFilterQuery($raw_sources){
  if($raw_sources=="all"){
    $final_query = "true";
  } else {
    $source_names = explode(",",$raw_sources);
    $final_query = "";
    for($i=0;$i<count($source_names);$i++){
      if($i==0){
        $final_query.="available_sources ";
      }else {
        $final_query.="OR available_sources ";
      }
      $final_query.="LIKE '%{$source_names[$i]}%' ";

    }
  }
  return $final_query;
}

if (isset($_GET['type']) && $_GET['type'] === 'shows') {
    $_type = 'shows';
    $_img_table = 'shows_to_images';
    $_table_id = 'show_id';
}
$sources_query = constructFilterQuery($sources);
// print_r($sources_query);
// die();
$query = "SELECT * FROM {$_type} WHERE ({$sources_query}) AND guidebox_rating IS NOT NULL ORDER BY `{$orderBy}` ASC LIMIT {$start}, {$amount}";

$results = expand($db->query($query, 'fetchAllRows'));

$data = [];
foreach ($results as $arr => $coll) {
    $data[$arr] = expand($coll);
}


for ($i = count($data) - 1; $i >= 0; $i--) {
    $data[$i]['posters'] = stripGuideboxDomain(expand($db->query("SELECT * FROM {$_img_table} WHERE {$_table_id} = {$data[$i]['id']} LIMiT 1")));
}


echo json_encode($data);
