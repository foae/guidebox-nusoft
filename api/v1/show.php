<?php


header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require_once 'db.php';
require_once 'helper.php';

$id = '';
$data = [];

if (!empty($_GET['id'])) {
    $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
} else {
    http_response_code(400);
    $output['message'] = 'No movie ID.';
}


/*
 * Basic movie information
 */
$data['basic_info'] = expand($db->query("select * from shows where id = {$id}"));

if (empty($data)) {
    http_response_code(404);
    $data['message'] = 'Show not found in database.';
    echo json_encode($data);
    die();
}

// -- Expanding -- //

/*
 * Phase 1
 */
$coll1 = [
    'shows_to_channels',
    'shows_to_genres',
    'shows_to_images',
];

foreach ($coll1 as $coll) {
    $key = explode('shows_to_', $coll);
    $data[$key[1]] = expand($db->query("select * from {$coll} where show_id = {$id}"));
}

/*
 * Phase 2
 */
$coll2 = [
    'shows_to_related',
    'shows_to_seasons',
];

foreach ($coll2 as $coll) {
    $key = explode('shows_to_', $coll);
    $data[$key[1]] = expand($db->query("select * from {$coll} where show_id = {$id}", 'fetchAllRows'));
}


/*
 * Shows to cast
 */
$data['cast'] = expand($db->query(
    "
      select * from shows_to_cast as c
       inner join persons as s on s.id = c.person_id
      where c.show_id = {$id}
    "
    , 'fetchAllRows'
));

// Expand additional fields
for ($i = count($data['cast']) - 1; $i >= 0; $i--) {
    $data['cast'][$i]['social'] = expand($data['cast'][$i]['social']);
}


/*
 * Shows to episodes
 */
$data['episodes'] = expand($db->query("select * from episodes where show_id = {$id}", 'fetchAllRows'));

$sources = [
    'episodes_sources_free_android',
    'episodes_sources_free_ios',
    'episodes_sources_free_web',
    // Formats:
    'episodes_sources_purchase_android',
    'episodes_sources_purchase_ios',
    'episodes_sources_purchase_web',
    'episodes_sources_subscription_android',
    'episodes_sources_subscription_ios',
    'episodes_sources_subscription_web',
    'episodes_sources_tv_everywhere_android',
    'episodes_sources_tv_everywhere_ios',
    'episodes_sources_tv_everywhere_web',
];

for ($i = count($data['episodes']) - 1; $i >= 0; $i--) {
    // Expand a lil'
    $data['episodes'][$i]['alternate_tvdb'] = expand($data['episodes'][$i]['alternate_tvdb']);
    $data['episodes'][$i]['alternate_titles'] = expand($data['episodes'][$i]['alternate_titles']);

    foreach ($sources as $source) {
        $key = explode('episodes_', $source)[1];
        $data['episodes'][$i][$key] = expand($db->query("select * from {$source} where episode_id = {$data['episodes'][$i]['id']}", 'fetchAllRows'));
    }
}

$data = stripGuideboxDomain($data);
echo json_encode($data);
die();