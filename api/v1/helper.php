<?php


/**
 * @param $data
 * @return string
 */
function debug($data)
{
    return PHP_EOL . print_r($data, true) . PHP_EOL;
}


/**
 * @param $data
 * @return string
 */
function clean($data)
{

    if ($data == '[]' || empty($data)) {
        $data = '';
    }

    return $data;

}


/**
 * @param $data
 * @return string
 */
function expand_string($data)
{

    if (is_string($data)) {
        if (json_decode($data)) {
            $data = json_decode($data, true);
        } else {
            $data = clean($data);
        }
    }

    return $data;
}


/**
 * @param $data
 * @return array
 */
function expand_array($data)
{

    $coll = [];

    if (is_array($data)) {
        foreach ($data as $key => $value) {

            if (is_json($value)) {
                $value = json_decode($value, true);

            } else {
                $value = expand_string($value);
            }

            $coll[$key] = $value;

        }
    }

    return $coll;
}


/**
 * @param $data
 * @return array|bool|string
 */
function expand($data)
{

    if (is_string($data)) {
        return expand_string($data);
    }

    if (is_array($data)) {
        return expand_array($data);
    }

    return false;
}


/**
 * @param $q
 * @return mixed
 */
function stripGuideboxDomain($q)
{

    $q = json_encode($q);
    $q = str_replace('http://static-api.guidebox.com/', '', $q);
    $q = str_replace('http:\/\/static-api.guidebox.com\/', '', $q);
    $q = json_decode($q, true);

    return $q;

}


/**
 * @param null $str
 * @return bool
 */
function is_json($str)
{
    if (is_string($str)) {
        @json_decode($str);
        return (json_last_error() === JSON_ERROR_NONE);
    }
    return false;
}