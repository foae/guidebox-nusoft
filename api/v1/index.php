<?php

echo 'Be more specific.';

/*
2) Title Page
a. Show/movie info
b. Watch links

Exemplu de URL propus:
api/v1/show?id=7856
api/v1/movie?id=734

Fiecare astfel de call ar trebui s intoarca un raspuns de tip JSON care sa includa toata informatia despre acel show/movie in db inclusiv:
---
titlu
poster image
summary
age rating
other ratings (RT,IMDB, if any)
release date,
length
cast, directors, writers (with url to pictures)
summary
similar shows/movies
availability/playback options INCLUSIV LINKURI:
subscription
purchase/rent
free
tv_anywhere
---

 *
 *
 *

Pentru seriale:
---
sezoane si episoade in fiecare
fiecare obiect episod cu:
episode nr.
title
description
poster url
availability (subs, purchase/rent, free, tv_anywhere  INCLUSIV LINKURI)
---
 */