<?php
//API Call to list all sources from the sources table. No params required
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require_once 'db.php';
require_once 'helper.php';
$_type = "sources";
$query = "SELECT * FROM {$_type}";

$results = $db->query($query, 'fetchAllRows');
$clean_response = [];
foreach($results as $source){
  $clean_response[$source['source']]=$source;
}



// $data = [];
// foreach ($results as $arr => $coll) {
//     $data[$arr] = expand($coll);
// }

echo json_encode($clean_response);
