<?php

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require_once 'db.php';
require_once 'helper.php';

$id = '';
$output = [];
$data = [];

if (!empty($_GET['id'])) {
    $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
} else {
    http_response_code(400);
    $output['error'] = 'No movie ID passed';
    echo json_encode($output);
    die();
}


/*
 * Basic movie information
 */
$data['basic_info'] = expand($db->query("select * from movies where id = {$id}"));


if (empty($data)) {
    http_response_code(404);
    $data['message'] = 'Movie not found in database.';
    echo json_encode($data);
    die();
}


/*
 * Movie images
 */
$data['images'] = expand($db->query("select * from movies_images where movie_id = {$id}"));

/*
 * Movie trailers
 */
$data['trailers'] = expand($db->query("select * from movies_trailers where movie_id = {$id}"));

/* -----------------------------------------------------------------------------------------
 * Movies sources
 * -----------------------------------------------------------------------------------------
 */
$tables_sources = [
    'movies_sources_free_android',
    'movies_sources_free_ios',
    'movies_sources_free_web',
    'movies_sources_other',
    'movies_sources_purchase_android',
    'movies_sources_purchase_ios',
    'movies_sources_purchase_web',
    'movies_sources_subscription_android',
    'movies_sources_subscription_ios',
    'movies_sources_subscription_web',
    'movies_sources_tv_everywhere_android',
    'movies_sources_tv_everywhere_ios',
    'movies_sources_tv_everywhere_web',
];


$_coll = [];
foreach ($tables_sources as $table) {
    $key = explode('movies_', $table);
    $_temp = $db->query("select * from {$table} where movie_id = {$id}", 'fetchAllRows');
    $data[$key[1]] = expand($_temp);
}

/*
 * -----------------------------------------------------------------------------------------
 * Movies -> persons
 * -----------------------------------------------------------------------------------------
 */

/*
 * movies_to_cast
 */
$data['cast'] = expand($db->query(
    "
        select * from movies_to_cast as t
        inner join persons as p on p.id = t.person_id
        where t.movie_id = {$id}
        ", 'fetchAllRows'
));

//foreach ($data['cast'] as $arr => $val) {
//    $data['cast'][$arr] = expand($db->query("select * from persons where id = {$val['person_id']}"));
//}


/*
 * movies_to_directors
 */
$data['directors'] = expand($db->query(
    "
        select * from movies_to_directors as t
        inner join persons as p on p.id = t.person_id
        where t.movie_id = {$id}
        ", 'fetchAllRows'
));


/*
 * movies_to_writers
 */
$data['writers'] = expand($db->query(
    "
        select * from movies_to_writers as t
        inner join persons as p on p.id = t.person_id
        where t.movie_id = {$id}
        ", 'fetchAllRows'
));

$data = stripGuideboxDomain($data);
echo json_encode($data);
die();