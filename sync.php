<?php

$url = 'http://guidebox.plainsight.ro/store/sync';

$ch = curl_init();
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($ch, CURLOPT_URL, $url);
curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 2);
curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

// Only calling the head
curl_setopt($ch, CURLOPT_HEADER, true); // header will be at output

//HERE IS THE MAGIC LINE
curl_setopt($ch, CURLOPT_NOBODY, true); // HTTP request is 'HEAD'

$content = curl_exec($ch);
curl_close ($ch);